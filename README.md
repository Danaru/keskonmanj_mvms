# keskonmanj

Un poc pour lancer des sondages afin de décider ce que l'on va manger le midi !

L'ensemble des données est mockée, les données saisies par l'utilisateur sont stocké en RAM.
Lorsque l'application est relancée, toutes les informations sont remises à 0.

Toute l'intelligence est embarquée dans l'application, pour une version publiable il faut mettre en place un backend avec une api et déporter l'ensemble de l'intelligence dans le backend.

Quelques tests sont écrits afin de démontrer la mise en place des TU dans Flutter.

**Temps mis pour la réalisation**: ~2 jours.

## Getting Started

Android minimal version: 4.3

Compte utilisateur pour la connexion: **test / testpass**



## Demo vidéo
![](demo_keskonmanj.mp4)

## APK a télécharger
Release: https://uprefer.jfrog.io/artifactory/keskonmanj/release/keskonmanj-latest.apk

Develop: https://uprefer.jfrog.io/artifactory/keskonmanj/develop/keskonmanj-latest.apk

## Coverage
Dispo ici: https://danaru.gitlab.io/keskonmanj_mvms
