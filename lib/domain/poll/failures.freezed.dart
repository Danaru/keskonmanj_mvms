// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PollFailureTearOff {
  const _$PollFailureTearOff();

  _PollUnknown unknown(dynamic internalError) {
    return _PollUnknown(
      internalError,
    );
  }

  _PollYetExistForThisDay yetExistForThisDay(DateTime day) {
    return _PollYetExistForThisDay(
      day,
    );
  }

  _NotFound notFound() {
    return const _NotFound();
  }
}

/// @nodoc
const $PollFailure = _$PollFailureTearOff();

/// @nodoc
mixin _$PollFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
    required TResult Function(DateTime day) yetExistForThisDay,
    required TResult Function() notFound,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    TResult Function(DateTime day)? yetExistForThisDay,
    TResult Function()? notFound,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_PollUnknown value) unknown,
    required TResult Function(_PollYetExistForThisDay value) yetExistForThisDay,
    required TResult Function(_NotFound value) notFound,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_PollUnknown value)? unknown,
    TResult Function(_PollYetExistForThisDay value)? yetExistForThisDay,
    TResult Function(_NotFound value)? notFound,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PollFailureCopyWith<$Res> {
  factory $PollFailureCopyWith(
          PollFailure value, $Res Function(PollFailure) then) =
      _$PollFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$PollFailureCopyWithImpl<$Res> implements $PollFailureCopyWith<$Res> {
  _$PollFailureCopyWithImpl(this._value, this._then);

  final PollFailure _value;
  // ignore: unused_field
  final $Res Function(PollFailure) _then;
}

/// @nodoc
abstract class _$PollUnknownCopyWith<$Res> {
  factory _$PollUnknownCopyWith(
          _PollUnknown value, $Res Function(_PollUnknown) then) =
      __$PollUnknownCopyWithImpl<$Res>;
  $Res call({dynamic internalError});
}

/// @nodoc
class __$PollUnknownCopyWithImpl<$Res> extends _$PollFailureCopyWithImpl<$Res>
    implements _$PollUnknownCopyWith<$Res> {
  __$PollUnknownCopyWithImpl(
      _PollUnknown _value, $Res Function(_PollUnknown) _then)
      : super(_value, (v) => _then(v as _PollUnknown));

  @override
  _PollUnknown get _value => super._value as _PollUnknown;

  @override
  $Res call({
    Object? internalError = freezed,
  }) {
    return _then(_PollUnknown(
      internalError == freezed
          ? _value.internalError
          : internalError // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$_PollUnknown implements _PollUnknown {
  const _$_PollUnknown(this.internalError);

  @override
  final dynamic internalError;

  @override
  String toString() {
    return 'PollFailure.unknown(internalError: $internalError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PollUnknown &&
            (identical(other.internalError, internalError) ||
                const DeepCollectionEquality()
                    .equals(other.internalError, internalError)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(internalError);

  @JsonKey(ignore: true)
  @override
  _$PollUnknownCopyWith<_PollUnknown> get copyWith =>
      __$PollUnknownCopyWithImpl<_PollUnknown>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
    required TResult Function(DateTime day) yetExistForThisDay,
    required TResult Function() notFound,
  }) {
    return unknown(internalError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    TResult Function(DateTime day)? yetExistForThisDay,
    TResult Function()? notFound,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(internalError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_PollUnknown value) unknown,
    required TResult Function(_PollYetExistForThisDay value) yetExistForThisDay,
    required TResult Function(_NotFound value) notFound,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_PollUnknown value)? unknown,
    TResult Function(_PollYetExistForThisDay value)? yetExistForThisDay,
    TResult Function(_NotFound value)? notFound,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }
}

abstract class _PollUnknown implements PollFailure {
  const factory _PollUnknown(dynamic internalError) = _$_PollUnknown;

  dynamic get internalError => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$PollUnknownCopyWith<_PollUnknown> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$PollYetExistForThisDayCopyWith<$Res> {
  factory _$PollYetExistForThisDayCopyWith(_PollYetExistForThisDay value,
          $Res Function(_PollYetExistForThisDay) then) =
      __$PollYetExistForThisDayCopyWithImpl<$Res>;
  $Res call({DateTime day});
}

/// @nodoc
class __$PollYetExistForThisDayCopyWithImpl<$Res>
    extends _$PollFailureCopyWithImpl<$Res>
    implements _$PollYetExistForThisDayCopyWith<$Res> {
  __$PollYetExistForThisDayCopyWithImpl(_PollYetExistForThisDay _value,
      $Res Function(_PollYetExistForThisDay) _then)
      : super(_value, (v) => _then(v as _PollYetExistForThisDay));

  @override
  _PollYetExistForThisDay get _value => super._value as _PollYetExistForThisDay;

  @override
  $Res call({
    Object? day = freezed,
  }) {
    return _then(_PollYetExistForThisDay(
      day == freezed
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$_PollYetExistForThisDay implements _PollYetExistForThisDay {
  const _$_PollYetExistForThisDay(this.day);

  @override
  final DateTime day;

  @override
  String toString() {
    return 'PollFailure.yetExistForThisDay(day: $day)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _PollYetExistForThisDay &&
            (identical(other.day, day) ||
                const DeepCollectionEquality().equals(other.day, day)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(day);

  @JsonKey(ignore: true)
  @override
  _$PollYetExistForThisDayCopyWith<_PollYetExistForThisDay> get copyWith =>
      __$PollYetExistForThisDayCopyWithImpl<_PollYetExistForThisDay>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
    required TResult Function(DateTime day) yetExistForThisDay,
    required TResult Function() notFound,
  }) {
    return yetExistForThisDay(day);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    TResult Function(DateTime day)? yetExistForThisDay,
    TResult Function()? notFound,
    required TResult orElse(),
  }) {
    if (yetExistForThisDay != null) {
      return yetExistForThisDay(day);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_PollUnknown value) unknown,
    required TResult Function(_PollYetExistForThisDay value) yetExistForThisDay,
    required TResult Function(_NotFound value) notFound,
  }) {
    return yetExistForThisDay(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_PollUnknown value)? unknown,
    TResult Function(_PollYetExistForThisDay value)? yetExistForThisDay,
    TResult Function(_NotFound value)? notFound,
    required TResult orElse(),
  }) {
    if (yetExistForThisDay != null) {
      return yetExistForThisDay(this);
    }
    return orElse();
  }
}

abstract class _PollYetExistForThisDay implements PollFailure {
  const factory _PollYetExistForThisDay(DateTime day) =
      _$_PollYetExistForThisDay;

  DateTime get day => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$PollYetExistForThisDayCopyWith<_PollYetExistForThisDay> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$NotFoundCopyWith<$Res> {
  factory _$NotFoundCopyWith(_NotFound value, $Res Function(_NotFound) then) =
      __$NotFoundCopyWithImpl<$Res>;
}

/// @nodoc
class __$NotFoundCopyWithImpl<$Res> extends _$PollFailureCopyWithImpl<$Res>
    implements _$NotFoundCopyWith<$Res> {
  __$NotFoundCopyWithImpl(_NotFound _value, $Res Function(_NotFound) _then)
      : super(_value, (v) => _then(v as _NotFound));

  @override
  _NotFound get _value => super._value as _NotFound;
}

/// @nodoc

class _$_NotFound implements _NotFound {
  const _$_NotFound();

  @override
  String toString() {
    return 'PollFailure.notFound()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _NotFound);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
    required TResult Function(DateTime day) yetExistForThisDay,
    required TResult Function() notFound,
  }) {
    return notFound();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    TResult Function(DateTime day)? yetExistForThisDay,
    TResult Function()? notFound,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_PollUnknown value) unknown,
    required TResult Function(_PollYetExistForThisDay value) yetExistForThisDay,
    required TResult Function(_NotFound value) notFound,
  }) {
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_PollUnknown value)? unknown,
    TResult Function(_PollYetExistForThisDay value)? yetExistForThisDay,
    TResult Function(_NotFound value)? notFound,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }
}

abstract class _NotFound implements PollFailure {
  const factory _NotFound() = _$_NotFound;
}

/// @nodoc
class _$RestaurantFailureTearOff {
  const _$RestaurantFailureTearOff();

  _RestaurantUnknown unknown(dynamic internalError) {
    return _RestaurantUnknown(
      internalError,
    );
  }
}

/// @nodoc
const $RestaurantFailure = _$RestaurantFailureTearOff();

/// @nodoc
mixin _$RestaurantFailure {
  dynamic get internalError => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_RestaurantUnknown value) unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_RestaurantUnknown value)? unknown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RestaurantFailureCopyWith<RestaurantFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RestaurantFailureCopyWith<$Res> {
  factory $RestaurantFailureCopyWith(
          RestaurantFailure value, $Res Function(RestaurantFailure) then) =
      _$RestaurantFailureCopyWithImpl<$Res>;
  $Res call({dynamic internalError});
}

/// @nodoc
class _$RestaurantFailureCopyWithImpl<$Res>
    implements $RestaurantFailureCopyWith<$Res> {
  _$RestaurantFailureCopyWithImpl(this._value, this._then);

  final RestaurantFailure _value;
  // ignore: unused_field
  final $Res Function(RestaurantFailure) _then;

  @override
  $Res call({
    Object? internalError = freezed,
  }) {
    return _then(_value.copyWith(
      internalError: internalError == freezed
          ? _value.internalError
          : internalError // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
abstract class _$RestaurantUnknownCopyWith<$Res>
    implements $RestaurantFailureCopyWith<$Res> {
  factory _$RestaurantUnknownCopyWith(
          _RestaurantUnknown value, $Res Function(_RestaurantUnknown) then) =
      __$RestaurantUnknownCopyWithImpl<$Res>;
  @override
  $Res call({dynamic internalError});
}

/// @nodoc
class __$RestaurantUnknownCopyWithImpl<$Res>
    extends _$RestaurantFailureCopyWithImpl<$Res>
    implements _$RestaurantUnknownCopyWith<$Res> {
  __$RestaurantUnknownCopyWithImpl(
      _RestaurantUnknown _value, $Res Function(_RestaurantUnknown) _then)
      : super(_value, (v) => _then(v as _RestaurantUnknown));

  @override
  _RestaurantUnknown get _value => super._value as _RestaurantUnknown;

  @override
  $Res call({
    Object? internalError = freezed,
  }) {
    return _then(_RestaurantUnknown(
      internalError == freezed
          ? _value.internalError
          : internalError // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$_RestaurantUnknown implements _RestaurantUnknown {
  const _$_RestaurantUnknown(this.internalError);

  @override
  final dynamic internalError;

  @override
  String toString() {
    return 'RestaurantFailure.unknown(internalError: $internalError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RestaurantUnknown &&
            (identical(other.internalError, internalError) ||
                const DeepCollectionEquality()
                    .equals(other.internalError, internalError)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(internalError);

  @JsonKey(ignore: true)
  @override
  _$RestaurantUnknownCopyWith<_RestaurantUnknown> get copyWith =>
      __$RestaurantUnknownCopyWithImpl<_RestaurantUnknown>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
  }) {
    return unknown(internalError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(internalError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_RestaurantUnknown value) unknown,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_RestaurantUnknown value)? unknown,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }
}

abstract class _RestaurantUnknown implements RestaurantFailure {
  const factory _RestaurantUnknown(dynamic internalError) =
      _$_RestaurantUnknown;

  @override
  dynamic get internalError => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RestaurantUnknownCopyWith<_RestaurantUnknown> get copyWith =>
      throw _privateConstructorUsedError;
}
