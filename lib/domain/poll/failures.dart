import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class PollFailure with _$PollFailure {
  const factory PollFailure.unknown(dynamic internalError) = _PollUnknown;

  const factory PollFailure.yetExistForThisDay(DateTime day) =
      _PollYetExistForThisDay;

  const factory PollFailure.notFound() = _NotFound;
}

@freezed
class RestaurantFailure with _$RestaurantFailure {
  const factory RestaurantFailure.unknown(dynamic internalError) =
      _RestaurantUnknown;
}
