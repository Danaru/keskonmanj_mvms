import 'package:dartz/dartz.dart';

import '../core/restaurant.dart';
import 'failures.dart';

abstract class IRestaurantFacade {
  Future<Either<RestaurantFailure, List<Restaurant>>> getAll();
}
