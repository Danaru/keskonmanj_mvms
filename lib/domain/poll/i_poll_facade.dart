import 'dart:async';

import 'package:dartz/dartz.dart';
import 'vote.dart';
import '../core/poll.dart';
import 'failures.dart';

abstract class IPollFacade {
  Stream get pollStream;

  Future<Either<PollFailure, List<Poll>>> getPolls();
  Future<Either<PollFailure, List<Poll>>> addPoll(Poll newPoll);
  Future<Either<PollFailure, Poll>> getPoll(String pollID);
  Future<Either<PollFailure, Poll>> vote(Poll poll, Vote vote);
  Future<Poll> closePoll(Poll poll);

  void dispose();
}
