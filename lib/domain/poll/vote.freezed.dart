// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'vote.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$VoteTearOff {
  const _$VoteTearOff();

  _Vote call({required String votantId, required Restaurant choice}) {
    return _Vote(
      votantId: votantId,
      choice: choice,
    );
  }
}

/// @nodoc
const $Vote = _$VoteTearOff();

/// @nodoc
mixin _$Vote {
  String get votantId => throw _privateConstructorUsedError;
  Restaurant get choice => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $VoteCopyWith<Vote> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VoteCopyWith<$Res> {
  factory $VoteCopyWith(Vote value, $Res Function(Vote) then) =
      _$VoteCopyWithImpl<$Res>;
  $Res call({String votantId, Restaurant choice});

  $RestaurantCopyWith<$Res> get choice;
}

/// @nodoc
class _$VoteCopyWithImpl<$Res> implements $VoteCopyWith<$Res> {
  _$VoteCopyWithImpl(this._value, this._then);

  final Vote _value;
  // ignore: unused_field
  final $Res Function(Vote) _then;

  @override
  $Res call({
    Object? votantId = freezed,
    Object? choice = freezed,
  }) {
    return _then(_value.copyWith(
      votantId: votantId == freezed
          ? _value.votantId
          : votantId // ignore: cast_nullable_to_non_nullable
              as String,
      choice: choice == freezed
          ? _value.choice
          : choice // ignore: cast_nullable_to_non_nullable
              as Restaurant,
    ));
  }

  @override
  $RestaurantCopyWith<$Res> get choice {
    return $RestaurantCopyWith<$Res>(_value.choice, (value) {
      return _then(_value.copyWith(choice: value));
    });
  }
}

/// @nodoc
abstract class _$VoteCopyWith<$Res> implements $VoteCopyWith<$Res> {
  factory _$VoteCopyWith(_Vote value, $Res Function(_Vote) then) =
      __$VoteCopyWithImpl<$Res>;
  @override
  $Res call({String votantId, Restaurant choice});

  @override
  $RestaurantCopyWith<$Res> get choice;
}

/// @nodoc
class __$VoteCopyWithImpl<$Res> extends _$VoteCopyWithImpl<$Res>
    implements _$VoteCopyWith<$Res> {
  __$VoteCopyWithImpl(_Vote _value, $Res Function(_Vote) _then)
      : super(_value, (v) => _then(v as _Vote));

  @override
  _Vote get _value => super._value as _Vote;

  @override
  $Res call({
    Object? votantId = freezed,
    Object? choice = freezed,
  }) {
    return _then(_Vote(
      votantId: votantId == freezed
          ? _value.votantId
          : votantId // ignore: cast_nullable_to_non_nullable
              as String,
      choice: choice == freezed
          ? _value.choice
          : choice // ignore: cast_nullable_to_non_nullable
              as Restaurant,
    ));
  }
}

/// @nodoc

class _$_Vote implements _Vote {
  const _$_Vote({required this.votantId, required this.choice});

  @override
  final String votantId;
  @override
  final Restaurant choice;

  @override
  String toString() {
    return 'Vote(votantId: $votantId, choice: $choice)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Vote &&
            (identical(other.votantId, votantId) ||
                const DeepCollectionEquality()
                    .equals(other.votantId, votantId)) &&
            (identical(other.choice, choice) ||
                const DeepCollectionEquality().equals(other.choice, choice)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(votantId) ^
      const DeepCollectionEquality().hash(choice);

  @JsonKey(ignore: true)
  @override
  _$VoteCopyWith<_Vote> get copyWith =>
      __$VoteCopyWithImpl<_Vote>(this, _$identity);
}

abstract class _Vote implements Vote {
  const factory _Vote({required String votantId, required Restaurant choice}) =
      _$_Vote;

  @override
  String get votantId => throw _privateConstructorUsedError;
  @override
  Restaurant get choice => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$VoteCopyWith<_Vote> get copyWith => throw _privateConstructorUsedError;
}
