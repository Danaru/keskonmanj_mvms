import 'package:freezed_annotation/freezed_annotation.dart';
import '../core/restaurant.dart';

part 'vote.freezed.dart';

@freezed
class Vote with _$Vote {
  const factory Vote({required String votantId, required Restaurant choice}) =
      _Vote;
}
