import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class RegisterAccountFailure with _$RegisterAccountFailure {
  const factory RegisterAccountFailure.unknown(dynamic internalError) =
      _RegisterUnknown;
}

@freezed
class SignInFailure with _$SignInFailure {
  const factory SignInFailure.unknown(dynamic internalError) = _SignInUnknown;

  const factory SignInFailure.badCredentials() = _BadCredentials;
}
