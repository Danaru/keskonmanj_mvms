import 'package:dartz/dartz.dart';

import '../core/user.dart';
import 'failures.dart';

abstract class IAuthFacade {
  Future<Either<RegisterAccountFailure, User>> registerAccount({
    required String email,
    required String username,
    required String password,
  });

  Future<Either<SignInFailure, User>> signIn(
      {required String username, required String password});

  Future<Either<SignInFailure, User>> autoLogin();

  Future<void> logout();
}
