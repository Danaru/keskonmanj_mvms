// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$RegisterAccountFailureTearOff {
  const _$RegisterAccountFailureTearOff();

  _RegisterUnknown unknown(dynamic internalError) {
    return _RegisterUnknown(
      internalError,
    );
  }
}

/// @nodoc
const $RegisterAccountFailure = _$RegisterAccountFailureTearOff();

/// @nodoc
mixin _$RegisterAccountFailure {
  dynamic get internalError => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_RegisterUnknown value) unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_RegisterUnknown value)? unknown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $RegisterAccountFailureCopyWith<RegisterAccountFailure> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegisterAccountFailureCopyWith<$Res> {
  factory $RegisterAccountFailureCopyWith(RegisterAccountFailure value,
          $Res Function(RegisterAccountFailure) then) =
      _$RegisterAccountFailureCopyWithImpl<$Res>;
  $Res call({dynamic internalError});
}

/// @nodoc
class _$RegisterAccountFailureCopyWithImpl<$Res>
    implements $RegisterAccountFailureCopyWith<$Res> {
  _$RegisterAccountFailureCopyWithImpl(this._value, this._then);

  final RegisterAccountFailure _value;
  // ignore: unused_field
  final $Res Function(RegisterAccountFailure) _then;

  @override
  $Res call({
    Object? internalError = freezed,
  }) {
    return _then(_value.copyWith(
      internalError: internalError == freezed
          ? _value.internalError
          : internalError // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc
abstract class _$RegisterUnknownCopyWith<$Res>
    implements $RegisterAccountFailureCopyWith<$Res> {
  factory _$RegisterUnknownCopyWith(
          _RegisterUnknown value, $Res Function(_RegisterUnknown) then) =
      __$RegisterUnknownCopyWithImpl<$Res>;
  @override
  $Res call({dynamic internalError});
}

/// @nodoc
class __$RegisterUnknownCopyWithImpl<$Res>
    extends _$RegisterAccountFailureCopyWithImpl<$Res>
    implements _$RegisterUnknownCopyWith<$Res> {
  __$RegisterUnknownCopyWithImpl(
      _RegisterUnknown _value, $Res Function(_RegisterUnknown) _then)
      : super(_value, (v) => _then(v as _RegisterUnknown));

  @override
  _RegisterUnknown get _value => super._value as _RegisterUnknown;

  @override
  $Res call({
    Object? internalError = freezed,
  }) {
    return _then(_RegisterUnknown(
      internalError == freezed
          ? _value.internalError
          : internalError // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$_RegisterUnknown implements _RegisterUnknown {
  const _$_RegisterUnknown(this.internalError);

  @override
  final dynamic internalError;

  @override
  String toString() {
    return 'RegisterAccountFailure.unknown(internalError: $internalError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _RegisterUnknown &&
            (identical(other.internalError, internalError) ||
                const DeepCollectionEquality()
                    .equals(other.internalError, internalError)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(internalError);

  @JsonKey(ignore: true)
  @override
  _$RegisterUnknownCopyWith<_RegisterUnknown> get copyWith =>
      __$RegisterUnknownCopyWithImpl<_RegisterUnknown>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
  }) {
    return unknown(internalError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(internalError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_RegisterUnknown value) unknown,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_RegisterUnknown value)? unknown,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }
}

abstract class _RegisterUnknown implements RegisterAccountFailure {
  const factory _RegisterUnknown(dynamic internalError) = _$_RegisterUnknown;

  @override
  dynamic get internalError => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$RegisterUnknownCopyWith<_RegisterUnknown> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
class _$SignInFailureTearOff {
  const _$SignInFailureTearOff();

  _SignInUnknown unknown(dynamic internalError) {
    return _SignInUnknown(
      internalError,
    );
  }

  _BadCredentials badCredentials() {
    return const _BadCredentials();
  }
}

/// @nodoc
const $SignInFailure = _$SignInFailureTearOff();

/// @nodoc
mixin _$SignInFailure {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
    required TResult Function() badCredentials,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignInUnknown value) unknown,
    required TResult Function(_BadCredentials value) badCredentials,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignInUnknown value)? unknown,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignInFailureCopyWith<$Res> {
  factory $SignInFailureCopyWith(
          SignInFailure value, $Res Function(SignInFailure) then) =
      _$SignInFailureCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignInFailureCopyWithImpl<$Res>
    implements $SignInFailureCopyWith<$Res> {
  _$SignInFailureCopyWithImpl(this._value, this._then);

  final SignInFailure _value;
  // ignore: unused_field
  final $Res Function(SignInFailure) _then;
}

/// @nodoc
abstract class _$SignInUnknownCopyWith<$Res> {
  factory _$SignInUnknownCopyWith(
          _SignInUnknown value, $Res Function(_SignInUnknown) then) =
      __$SignInUnknownCopyWithImpl<$Res>;
  $Res call({dynamic internalError});
}

/// @nodoc
class __$SignInUnknownCopyWithImpl<$Res>
    extends _$SignInFailureCopyWithImpl<$Res>
    implements _$SignInUnknownCopyWith<$Res> {
  __$SignInUnknownCopyWithImpl(
      _SignInUnknown _value, $Res Function(_SignInUnknown) _then)
      : super(_value, (v) => _then(v as _SignInUnknown));

  @override
  _SignInUnknown get _value => super._value as _SignInUnknown;

  @override
  $Res call({
    Object? internalError = freezed,
  }) {
    return _then(_SignInUnknown(
      internalError == freezed
          ? _value.internalError
          : internalError // ignore: cast_nullable_to_non_nullable
              as dynamic,
    ));
  }
}

/// @nodoc

class _$_SignInUnknown implements _SignInUnknown {
  const _$_SignInUnknown(this.internalError);

  @override
  final dynamic internalError;

  @override
  String toString() {
    return 'SignInFailure.unknown(internalError: $internalError)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SignInUnknown &&
            (identical(other.internalError, internalError) ||
                const DeepCollectionEquality()
                    .equals(other.internalError, internalError)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(internalError);

  @JsonKey(ignore: true)
  @override
  _$SignInUnknownCopyWith<_SignInUnknown> get copyWith =>
      __$SignInUnknownCopyWithImpl<_SignInUnknown>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
    required TResult Function() badCredentials,
  }) {
    return unknown(internalError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(internalError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignInUnknown value) unknown,
    required TResult Function(_BadCredentials value) badCredentials,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignInUnknown value)? unknown,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }
}

abstract class _SignInUnknown implements SignInFailure {
  const factory _SignInUnknown(dynamic internalError) = _$_SignInUnknown;

  dynamic get internalError => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$SignInUnknownCopyWith<_SignInUnknown> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$BadCredentialsCopyWith<$Res> {
  factory _$BadCredentialsCopyWith(
          _BadCredentials value, $Res Function(_BadCredentials) then) =
      __$BadCredentialsCopyWithImpl<$Res>;
}

/// @nodoc
class __$BadCredentialsCopyWithImpl<$Res>
    extends _$SignInFailureCopyWithImpl<$Res>
    implements _$BadCredentialsCopyWith<$Res> {
  __$BadCredentialsCopyWithImpl(
      _BadCredentials _value, $Res Function(_BadCredentials) _then)
      : super(_value, (v) => _then(v as _BadCredentials));

  @override
  _BadCredentials get _value => super._value as _BadCredentials;
}

/// @nodoc

class _$_BadCredentials implements _BadCredentials {
  const _$_BadCredentials();

  @override
  String toString() {
    return 'SignInFailure.badCredentials()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _BadCredentials);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(dynamic internalError) unknown,
    required TResult Function() badCredentials,
  }) {
    return badCredentials();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(dynamic internalError)? unknown,
    TResult Function()? badCredentials,
    required TResult orElse(),
  }) {
    if (badCredentials != null) {
      return badCredentials();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignInUnknown value) unknown,
    required TResult Function(_BadCredentials value) badCredentials,
  }) {
    return badCredentials(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignInUnknown value)? unknown,
    TResult Function(_BadCredentials value)? badCredentials,
    required TResult orElse(),
  }) {
    if (badCredentials != null) {
      return badCredentials(this);
    }
    return orElse();
  }
}

abstract class _BadCredentials implements SignInFailure {
  const factory _BadCredentials() = _$_BadCredentials;
}
