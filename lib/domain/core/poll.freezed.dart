// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'poll.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PollTearOff {
  const _$PollTearOff();

  _Poll call(
      {required String id,
      required DateTime day,
      required List<Restaurant> choices,
      List<Vote>? votes,
      bool closed = false,
      List<VoteResult> results = const [],
      required User creator}) {
    return _Poll(
      id: id,
      day: day,
      choices: choices,
      votes: votes,
      closed: closed,
      results: results,
      creator: creator,
    );
  }
}

/// @nodoc
const $Poll = _$PollTearOff();

/// @nodoc
mixin _$Poll {
  String get id => throw _privateConstructorUsedError;
  DateTime get day => throw _privateConstructorUsedError;
  List<Restaurant> get choices => throw _privateConstructorUsedError;
  List<Vote>? get votes => throw _privateConstructorUsedError;
  bool get closed => throw _privateConstructorUsedError;
  List<VoteResult> get results => throw _privateConstructorUsedError;
  User get creator => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PollCopyWith<Poll> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PollCopyWith<$Res> {
  factory $PollCopyWith(Poll value, $Res Function(Poll) then) =
      _$PollCopyWithImpl<$Res>;
  $Res call(
      {String id,
      DateTime day,
      List<Restaurant> choices,
      List<Vote>? votes,
      bool closed,
      List<VoteResult> results,
      User creator});

  $UserCopyWith<$Res> get creator;
}

/// @nodoc
class _$PollCopyWithImpl<$Res> implements $PollCopyWith<$Res> {
  _$PollCopyWithImpl(this._value, this._then);

  final Poll _value;
  // ignore: unused_field
  final $Res Function(Poll) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? day = freezed,
    Object? choices = freezed,
    Object? votes = freezed,
    Object? closed = freezed,
    Object? results = freezed,
    Object? creator = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      day: day == freezed
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as DateTime,
      choices: choices == freezed
          ? _value.choices
          : choices // ignore: cast_nullable_to_non_nullable
              as List<Restaurant>,
      votes: votes == freezed
          ? _value.votes
          : votes // ignore: cast_nullable_to_non_nullable
              as List<Vote>?,
      closed: closed == freezed
          ? _value.closed
          : closed // ignore: cast_nullable_to_non_nullable
              as bool,
      results: results == freezed
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<VoteResult>,
      creator: creator == freezed
          ? _value.creator
          : creator // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  $UserCopyWith<$Res> get creator {
    return $UserCopyWith<$Res>(_value.creator, (value) {
      return _then(_value.copyWith(creator: value));
    });
  }
}

/// @nodoc
abstract class _$PollCopyWith<$Res> implements $PollCopyWith<$Res> {
  factory _$PollCopyWith(_Poll value, $Res Function(_Poll) then) =
      __$PollCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id,
      DateTime day,
      List<Restaurant> choices,
      List<Vote>? votes,
      bool closed,
      List<VoteResult> results,
      User creator});

  @override
  $UserCopyWith<$Res> get creator;
}

/// @nodoc
class __$PollCopyWithImpl<$Res> extends _$PollCopyWithImpl<$Res>
    implements _$PollCopyWith<$Res> {
  __$PollCopyWithImpl(_Poll _value, $Res Function(_Poll) _then)
      : super(_value, (v) => _then(v as _Poll));

  @override
  _Poll get _value => super._value as _Poll;

  @override
  $Res call({
    Object? id = freezed,
    Object? day = freezed,
    Object? choices = freezed,
    Object? votes = freezed,
    Object? closed = freezed,
    Object? results = freezed,
    Object? creator = freezed,
  }) {
    return _then(_Poll(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      day: day == freezed
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as DateTime,
      choices: choices == freezed
          ? _value.choices
          : choices // ignore: cast_nullable_to_non_nullable
              as List<Restaurant>,
      votes: votes == freezed
          ? _value.votes
          : votes // ignore: cast_nullable_to_non_nullable
              as List<Vote>?,
      closed: closed == freezed
          ? _value.closed
          : closed // ignore: cast_nullable_to_non_nullable
              as bool,
      results: results == freezed
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<VoteResult>,
      creator: creator == freezed
          ? _value.creator
          : creator // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }
}

/// @nodoc

class _$_Poll extends _Poll {
  const _$_Poll(
      {required this.id,
      required this.day,
      required this.choices,
      this.votes,
      this.closed = false,
      this.results = const [],
      required this.creator})
      : super._();

  @override
  final String id;
  @override
  final DateTime day;
  @override
  final List<Restaurant> choices;
  @override
  final List<Vote>? votes;
  @JsonKey(defaultValue: false)
  @override
  final bool closed;
  @JsonKey(defaultValue: const [])
  @override
  final List<VoteResult> results;
  @override
  final User creator;

  @override
  String toString() {
    return 'Poll(id: $id, day: $day, choices: $choices, votes: $votes, closed: $closed, results: $results, creator: $creator)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Poll &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.day, day) ||
                const DeepCollectionEquality().equals(other.day, day)) &&
            (identical(other.choices, choices) ||
                const DeepCollectionEquality()
                    .equals(other.choices, choices)) &&
            (identical(other.votes, votes) ||
                const DeepCollectionEquality().equals(other.votes, votes)) &&
            (identical(other.closed, closed) ||
                const DeepCollectionEquality().equals(other.closed, closed)) &&
            (identical(other.results, results) ||
                const DeepCollectionEquality()
                    .equals(other.results, results)) &&
            (identical(other.creator, creator) ||
                const DeepCollectionEquality().equals(other.creator, creator)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(day) ^
      const DeepCollectionEquality().hash(choices) ^
      const DeepCollectionEquality().hash(votes) ^
      const DeepCollectionEquality().hash(closed) ^
      const DeepCollectionEquality().hash(results) ^
      const DeepCollectionEquality().hash(creator);

  @JsonKey(ignore: true)
  @override
  _$PollCopyWith<_Poll> get copyWith =>
      __$PollCopyWithImpl<_Poll>(this, _$identity);
}

abstract class _Poll extends Poll {
  const factory _Poll(
      {required String id,
      required DateTime day,
      required List<Restaurant> choices,
      List<Vote>? votes,
      bool closed,
      List<VoteResult> results,
      required User creator}) = _$_Poll;
  const _Poll._() : super._();

  @override
  String get id => throw _privateConstructorUsedError;
  @override
  DateTime get day => throw _privateConstructorUsedError;
  @override
  List<Restaurant> get choices => throw _privateConstructorUsedError;
  @override
  List<Vote>? get votes => throw _privateConstructorUsedError;
  @override
  bool get closed => throw _privateConstructorUsedError;
  @override
  List<VoteResult> get results => throw _privateConstructorUsedError;
  @override
  User get creator => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$PollCopyWith<_Poll> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
class _$VoteResultTearOff {
  const _$VoteResultTearOff();

  _VoteResults call(Restaurant restaurant, int points) {
    return _VoteResults(
      restaurant,
      points,
    );
  }
}

/// @nodoc
const $VoteResult = _$VoteResultTearOff();

/// @nodoc
mixin _$VoteResult {
  Restaurant get restaurant => throw _privateConstructorUsedError;
  int get points => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $VoteResultCopyWith<VoteResult> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VoteResultCopyWith<$Res> {
  factory $VoteResultCopyWith(
          VoteResult value, $Res Function(VoteResult) then) =
      _$VoteResultCopyWithImpl<$Res>;
  $Res call({Restaurant restaurant, int points});

  $RestaurantCopyWith<$Res> get restaurant;
}

/// @nodoc
class _$VoteResultCopyWithImpl<$Res> implements $VoteResultCopyWith<$Res> {
  _$VoteResultCopyWithImpl(this._value, this._then);

  final VoteResult _value;
  // ignore: unused_field
  final $Res Function(VoteResult) _then;

  @override
  $Res call({
    Object? restaurant = freezed,
    Object? points = freezed,
  }) {
    return _then(_value.copyWith(
      restaurant: restaurant == freezed
          ? _value.restaurant
          : restaurant // ignore: cast_nullable_to_non_nullable
              as Restaurant,
      points: points == freezed
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }

  @override
  $RestaurantCopyWith<$Res> get restaurant {
    return $RestaurantCopyWith<$Res>(_value.restaurant, (value) {
      return _then(_value.copyWith(restaurant: value));
    });
  }
}

/// @nodoc
abstract class _$VoteResultsCopyWith<$Res>
    implements $VoteResultCopyWith<$Res> {
  factory _$VoteResultsCopyWith(
          _VoteResults value, $Res Function(_VoteResults) then) =
      __$VoteResultsCopyWithImpl<$Res>;
  @override
  $Res call({Restaurant restaurant, int points});

  @override
  $RestaurantCopyWith<$Res> get restaurant;
}

/// @nodoc
class __$VoteResultsCopyWithImpl<$Res> extends _$VoteResultCopyWithImpl<$Res>
    implements _$VoteResultsCopyWith<$Res> {
  __$VoteResultsCopyWithImpl(
      _VoteResults _value, $Res Function(_VoteResults) _then)
      : super(_value, (v) => _then(v as _VoteResults));

  @override
  _VoteResults get _value => super._value as _VoteResults;

  @override
  $Res call({
    Object? restaurant = freezed,
    Object? points = freezed,
  }) {
    return _then(_VoteResults(
      restaurant == freezed
          ? _value.restaurant
          : restaurant // ignore: cast_nullable_to_non_nullable
              as Restaurant,
      points == freezed
          ? _value.points
          : points // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_VoteResults implements _VoteResults {
  const _$_VoteResults(this.restaurant, this.points);

  @override
  final Restaurant restaurant;
  @override
  final int points;

  @override
  String toString() {
    return 'VoteResult(restaurant: $restaurant, points: $points)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _VoteResults &&
            (identical(other.restaurant, restaurant) ||
                const DeepCollectionEquality()
                    .equals(other.restaurant, restaurant)) &&
            (identical(other.points, points) ||
                const DeepCollectionEquality().equals(other.points, points)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(restaurant) ^
      const DeepCollectionEquality().hash(points);

  @JsonKey(ignore: true)
  @override
  _$VoteResultsCopyWith<_VoteResults> get copyWith =>
      __$VoteResultsCopyWithImpl<_VoteResults>(this, _$identity);
}

abstract class _VoteResults implements VoteResult {
  const factory _VoteResults(Restaurant restaurant, int points) =
      _$_VoteResults;

  @override
  Restaurant get restaurant => throw _privateConstructorUsedError;
  @override
  int get points => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$VoteResultsCopyWith<_VoteResults> get copyWith =>
      throw _privateConstructorUsedError;
}
