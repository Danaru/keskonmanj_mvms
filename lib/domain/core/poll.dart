import 'package:freezed_annotation/freezed_annotation.dart';
import 'user.dart';
import '../poll/vote.dart';
import 'restaurant.dart';

part 'poll.freezed.dart';

@freezed
class Poll with _$Poll {
  const Poll._();
  const factory Poll(
      {required String id,
      required DateTime day,
      required List<Restaurant> choices,
      List<Vote>? votes,
      @Default(false) bool closed,
      @Default([]) List<VoteResult> results,
      required User creator}) = _Poll;

  bool get isActive {
    DateTime dateToCompare = DateTime(DateTime.now().year, DateTime.now().month,
        DateTime.now().day, 11, 30, 00);
    if (this.closed) {
      return false;
    }
    return day.compareTo(dateToCompare) >= 0;
  }

  Restaurant get winner {
    results.sort((a, b) => b.points.compareTo(a.points));
    return results.first.restaurant;
  }
}

@freezed
class VoteResult with _$VoteResult {
  const factory VoteResult(Restaurant restaurant, int points) = _VoteResults;
}
