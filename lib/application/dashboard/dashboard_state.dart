import 'package:freezed_annotation/freezed_annotation.dart';
import '../../domain/core/poll.dart';
import '../../domain/poll/failures.dart';
part 'dashboard_state.freezed.dart';

@freezed
class DashboardState with _$DashboardState {
  const factory DashboardState.loading() = _Loading;
  const factory DashboardState.loaded({required List<Poll> polls}) = _Loaded;

  const factory DashboardState.error(PollFailure fail) = _Error;
}
