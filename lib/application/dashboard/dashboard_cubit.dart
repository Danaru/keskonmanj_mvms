import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import '../../domain/poll/i_poll_facade.dart';
import 'dashboard_state.dart';

@injectable
class DashboardCubit extends Cubit<DashboardState> {
  final IPollFacade _pollFacade;
  DashboardCubit(this._pollFacade) : super(DashboardState.loading()) {
    _pollFacade.pollStream.listen((event) {
      load();
    });
  }

  void load() async {
    emit(DashboardState.loading());
    final failOrPolls = await _pollFacade.getPolls();
    failOrPolls.fold(
      (fail) => emit(DashboardState.error(fail)),
      (polls) => emit(DashboardState.loaded(polls: polls)),
    );
  }
}
