// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'sign_in_account_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$SignInAccountStateTearOff {
  const _$SignInAccountStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _Signing signing() {
    return const _Signing();
  }

  _Failed failed(SignInFailure failure) {
    return _Failed(
      failure,
    );
  }

  _Succeed succeed() {
    return const _Succeed();
  }
}

/// @nodoc
const $SignInAccountState = _$SignInAccountStateTearOff();

/// @nodoc
mixin _$SignInAccountState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() signing,
    required TResult Function(SignInFailure failure) failed,
    required TResult Function() succeed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? signing,
    TResult Function(SignInFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Signing value) signing,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Signing value)? signing,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SignInAccountStateCopyWith<$Res> {
  factory $SignInAccountStateCopyWith(
          SignInAccountState value, $Res Function(SignInAccountState) then) =
      _$SignInAccountStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SignInAccountStateCopyWithImpl<$Res>
    implements $SignInAccountStateCopyWith<$Res> {
  _$SignInAccountStateCopyWithImpl(this._value, this._then);

  final SignInAccountState _value;
  // ignore: unused_field
  final $Res Function(SignInAccountState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res>
    extends _$SignInAccountStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'SignInAccountState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() signing,
    required TResult Function(SignInFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? signing,
    TResult Function(SignInFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Signing value) signing,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Signing value)? signing,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements SignInAccountState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$SigningCopyWith<$Res> {
  factory _$SigningCopyWith(_Signing value, $Res Function(_Signing) then) =
      __$SigningCopyWithImpl<$Res>;
}

/// @nodoc
class __$SigningCopyWithImpl<$Res>
    extends _$SignInAccountStateCopyWithImpl<$Res>
    implements _$SigningCopyWith<$Res> {
  __$SigningCopyWithImpl(_Signing _value, $Res Function(_Signing) _then)
      : super(_value, (v) => _then(v as _Signing));

  @override
  _Signing get _value => super._value as _Signing;
}

/// @nodoc

class _$_Signing implements _Signing {
  const _$_Signing();

  @override
  String toString() {
    return 'SignInAccountState.signing()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Signing);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() signing,
    required TResult Function(SignInFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return signing();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? signing,
    TResult Function(SignInFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (signing != null) {
      return signing();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Signing value) signing,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return signing(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Signing value)? signing,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (signing != null) {
      return signing(this);
    }
    return orElse();
  }
}

abstract class _Signing implements SignInAccountState {
  const factory _Signing() = _$_Signing;
}

/// @nodoc
abstract class _$FailedCopyWith<$Res> {
  factory _$FailedCopyWith(_Failed value, $Res Function(_Failed) then) =
      __$FailedCopyWithImpl<$Res>;
  $Res call({SignInFailure failure});

  $SignInFailureCopyWith<$Res> get failure;
}

/// @nodoc
class __$FailedCopyWithImpl<$Res> extends _$SignInAccountStateCopyWithImpl<$Res>
    implements _$FailedCopyWith<$Res> {
  __$FailedCopyWithImpl(_Failed _value, $Res Function(_Failed) _then)
      : super(_value, (v) => _then(v as _Failed));

  @override
  _Failed get _value => super._value as _Failed;

  @override
  $Res call({
    Object? failure = freezed,
  }) {
    return _then(_Failed(
      failure == freezed
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as SignInFailure,
    ));
  }

  @override
  $SignInFailureCopyWith<$Res> get failure {
    return $SignInFailureCopyWith<$Res>(_value.failure, (value) {
      return _then(_value.copyWith(failure: value));
    });
  }
}

/// @nodoc

class _$_Failed implements _Failed {
  const _$_Failed(this.failure);

  @override
  final SignInFailure failure;

  @override
  String toString() {
    return 'SignInAccountState.failed(failure: $failure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Failed &&
            (identical(other.failure, failure) ||
                const DeepCollectionEquality().equals(other.failure, failure)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failure);

  @JsonKey(ignore: true)
  @override
  _$FailedCopyWith<_Failed> get copyWith =>
      __$FailedCopyWithImpl<_Failed>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() signing,
    required TResult Function(SignInFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return failed(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? signing,
    TResult Function(SignInFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Signing value) signing,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return failed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Signing value)? signing,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (failed != null) {
      return failed(this);
    }
    return orElse();
  }
}

abstract class _Failed implements SignInAccountState {
  const factory _Failed(SignInFailure failure) = _$_Failed;

  SignInFailure get failure => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$FailedCopyWith<_Failed> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$SucceedCopyWith<$Res> {
  factory _$SucceedCopyWith(_Succeed value, $Res Function(_Succeed) then) =
      __$SucceedCopyWithImpl<$Res>;
}

/// @nodoc
class __$SucceedCopyWithImpl<$Res>
    extends _$SignInAccountStateCopyWithImpl<$Res>
    implements _$SucceedCopyWith<$Res> {
  __$SucceedCopyWithImpl(_Succeed _value, $Res Function(_Succeed) _then)
      : super(_value, (v) => _then(v as _Succeed));

  @override
  _Succeed get _value => super._value as _Succeed;
}

/// @nodoc

class _$_Succeed implements _Succeed {
  const _$_Succeed();

  @override
  String toString() {
    return 'SignInAccountState.succeed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Succeed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() signing,
    required TResult Function(SignInFailure failure) failed,
    required TResult Function() succeed,
  }) {
    return succeed();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? signing,
    TResult Function(SignInFailure failure)? failed,
    TResult Function()? succeed,
    required TResult orElse(),
  }) {
    if (succeed != null) {
      return succeed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Signing value) signing,
    required TResult Function(_Failed value) failed,
    required TResult Function(_Succeed value) succeed,
  }) {
    return succeed(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Signing value)? signing,
    TResult Function(_Failed value)? failed,
    TResult Function(_Succeed value)? succeed,
    required TResult orElse(),
  }) {
    if (succeed != null) {
      return succeed(this);
    }
    return orElse();
  }
}

abstract class _Succeed implements SignInAccountState {
  const factory _Succeed() = _$_Succeed;
}
