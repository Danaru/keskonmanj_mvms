import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';

import '../../domain/auth/i_auth_facade.dart';
import '../core/injection.dart';
import '../core/user_session_cubit.dart';
import 'sign_in_account_state.dart';

@injectable
class SignInAccountCubit extends Cubit<SignInAccountState> {
  final IAuthFacade _authFacade;
  SignInAccountCubit(this._authFacade) : super(SignInAccountState.initial());

  void signIn({required String username, required String password}) async {
    emit(SignInAccountState.signing());
    final failOrSignedIn =
        await _authFacade.signIn(username: username, password: password);
    failOrSignedIn.fold(
      (failure) => emit(SignInAccountState.failed(failure)),
      (user) {
        getIt<UserSessionCubit>().updateCurrentUser(user);
        emit(SignInAccountState.succeed());
      },
    );
  }
}
