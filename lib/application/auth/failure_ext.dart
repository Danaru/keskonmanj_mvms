import '../../domain/auth/failures.dart';


extension RegisterFailureMessage on RegisterAccountFailure {
  String get message =>
      this.map(unknown: (fail) => fail.internalError.toString());
}

extension SignInFailureMessage on SignInFailure {
  String get message => this.map(
      unknown: (fail) => fail.internalError.toString(),
      badCredentials: (_) => "Mauvais nom d'utilisateur / mot de passe");
}
