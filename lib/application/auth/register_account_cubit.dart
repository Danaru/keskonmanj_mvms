import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';

import '../../domain/auth/i_auth_facade.dart';
import '../core/injection.dart';
import '../core/user_session_cubit.dart';
import 'register_account_state.dart';

@injectable
class RegisterAccountCubit extends Cubit<RegisterAccountState> {
  final IAuthFacade _authFacade;
  RegisterAccountCubit(this._authFacade)
      : super(RegisterAccountState.initial());

  void register({
    required String email,
    required String username,
    required String password,
  }) async {
    emit(RegisterAccountState.registering());
    final failOrRegistered = await _authFacade.registerAccount(
        email: email, username: username, password: password);
    failOrRegistered.fold(
      (fail) => emit(RegisterAccountState.failed(fail)),
      (user) {
        getIt<UserSessionCubit>().updateCurrentUser(user);
        emit(RegisterAccountState.succeed());
      },
    );
  }
}
