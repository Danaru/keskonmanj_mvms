import 'package:freezed_annotation/freezed_annotation.dart';

import '../../domain/auth/failures.dart';

part 'sign_in_account_state.freezed.dart';

@freezed
class SignInAccountState with _$SignInAccountState {
  const factory SignInAccountState.initial() = _Initial;
  const factory SignInAccountState.signing() = _Signing;

  const factory SignInAccountState.failed(SignInFailure failure) = _Failed;

  const factory SignInAccountState.succeed() = _Succeed;
}
