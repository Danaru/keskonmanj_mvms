import 'package:freezed_annotation/freezed_annotation.dart';

import '../../domain/core/user.dart';

part 'user_session_state.freezed.dart';

@freezed
class UserSessionState with _$UserSessionState {
  const factory UserSessionState.initial() = _Initial;
  const factory UserSessionState.authentified(User currentUser) = _Authentified;
  const factory UserSessionState.unauthentified() = _Unauthentified;
}
