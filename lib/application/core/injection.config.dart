// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../domain/auth/i_auth_facade.dart' as _i10;
import '../../domain/poll/i_poll_facade.dart' as _i12;
import '../../domain/poll/i_restaurant_facade.dart' as _i14;
import '../../infrastructure/auth/auth_facade.dart' as _i11;
import '../../infrastructure/auth/credentials_local_data_source.dart' as _i3;
import '../../infrastructure/auth/fake_user_remote_data_source.dart' as _i9;
import '../../infrastructure/auth/i_user_data_source.dart' as _i8;
import '../../infrastructure/core/fake_restaurant_remote_data_source.dart'
    as _i7;
import '../../infrastructure/core/i_restaurant_remote_data_source.dart' as _i6;
import '../../infrastructure/core/restaurant_facade.dart' as _i15;
import '../../infrastructure/polls/fake_poll_remote_data_source.dart' as _i5;
import '../../infrastructure/polls/i_poll_remote_data_source.dart' as _i4;
import '../../infrastructure/polls/polls_facade.dart' as _i13;
import '../auth/register_account_cubit.dart' as _i17;
import '../auth/sign_in_account_cubit.dart' as _i18;
import '../dashboard/dashboard_cubit.dart' as _i21;
import '../poll/new_poll_cubit.dart' as _i16;
import '../poll/vote_cubit.dart' as _i20;
import 'user_session_cubit.dart'
    as _i19; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.lazySingleton<_i3.CredentialsLocalDataSource>(
      () => _i3.CredentialsLocalDataSource());
  gh.lazySingleton<_i4.IPollRemoteDataSource>(
      () => _i5.FakePollRemoteDataSource());
  gh.lazySingleton<_i6.IRestaurantRemoteDataSource>(
      () => _i7.FakeRestaurantRemoteDataSource());
  gh.lazySingleton<_i8.IUserRemoteDataSource>(
      () => _i9.FakeUserRemoteDataSource());
  gh.lazySingleton<_i10.IAuthFacade>(() => _i11.AuthFacade(
      get<_i8.IUserRemoteDataSource>(), get<_i3.CredentialsLocalDataSource>()));
  gh.lazySingleton<_i12.IPollFacade>(
      () => _i13.PollsFacade(get<_i4.IPollRemoteDataSource>()),
      dispose: (i) => i.dispose());
  gh.lazySingleton<_i14.IRestaurantFacade>(
      () => _i15.RestaurantFacade(get<_i6.IRestaurantRemoteDataSource>()));
  gh.factory<_i16.NewPollCubit>(() => _i16.NewPollCubit(
      get<_i14.IRestaurantFacade>(), get<_i12.IPollFacade>()));
  gh.factory<_i17.RegisterAccountCubit>(
      () => _i17.RegisterAccountCubit(get<_i10.IAuthFacade>()));
  gh.factory<_i18.SignInAccountCubit>(
      () => _i18.SignInAccountCubit(get<_i10.IAuthFacade>()));
  gh.lazySingleton<_i19.UserSessionCubit>(
      () => _i19.UserSessionCubit(get<_i10.IAuthFacade>()));
  gh.factory<_i20.VoteCubit>(() => _i20.VoteCubit(get<_i12.IPollFacade>()));
  gh.factory<_i21.DashboardCubit>(
      () => _i21.DashboardCubit(get<_i12.IPollFacade>()));
  return get;
}
