// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'user_session_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$UserSessionStateTearOff {
  const _$UserSessionStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _Authentified authentified(User currentUser) {
    return _Authentified(
      currentUser,
    );
  }

  _Unauthentified unauthentified() {
    return const _Unauthentified();
  }
}

/// @nodoc
const $UserSessionState = _$UserSessionStateTearOff();

/// @nodoc
mixin _$UserSessionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(User currentUser) authentified,
    required TResult Function() unauthentified,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(User currentUser)? authentified,
    TResult Function()? unauthentified,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Authentified value) authentified,
    required TResult Function(_Unauthentified value) unauthentified,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Authentified value)? authentified,
    TResult Function(_Unauthentified value)? unauthentified,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserSessionStateCopyWith<$Res> {
  factory $UserSessionStateCopyWith(
          UserSessionState value, $Res Function(UserSessionState) then) =
      _$UserSessionStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserSessionStateCopyWithImpl<$Res>
    implements $UserSessionStateCopyWith<$Res> {
  _$UserSessionStateCopyWithImpl(this._value, this._then);

  final UserSessionState _value;
  // ignore: unused_field
  final $Res Function(UserSessionState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$UserSessionStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'UserSessionState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(User currentUser) authentified,
    required TResult Function() unauthentified,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(User currentUser)? authentified,
    TResult Function()? unauthentified,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Authentified value) authentified,
    required TResult Function(_Unauthentified value) unauthentified,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Authentified value)? authentified,
    TResult Function(_Unauthentified value)? unauthentified,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements UserSessionState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$AuthentifiedCopyWith<$Res> {
  factory _$AuthentifiedCopyWith(
          _Authentified value, $Res Function(_Authentified) then) =
      __$AuthentifiedCopyWithImpl<$Res>;
  $Res call({User currentUser});

  $UserCopyWith<$Res> get currentUser;
}

/// @nodoc
class __$AuthentifiedCopyWithImpl<$Res>
    extends _$UserSessionStateCopyWithImpl<$Res>
    implements _$AuthentifiedCopyWith<$Res> {
  __$AuthentifiedCopyWithImpl(
      _Authentified _value, $Res Function(_Authentified) _then)
      : super(_value, (v) => _then(v as _Authentified));

  @override
  _Authentified get _value => super._value as _Authentified;

  @override
  $Res call({
    Object? currentUser = freezed,
  }) {
    return _then(_Authentified(
      currentUser == freezed
          ? _value.currentUser
          : currentUser // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  $UserCopyWith<$Res> get currentUser {
    return $UserCopyWith<$Res>(_value.currentUser, (value) {
      return _then(_value.copyWith(currentUser: value));
    });
  }
}

/// @nodoc

class _$_Authentified implements _Authentified {
  const _$_Authentified(this.currentUser);

  @override
  final User currentUser;

  @override
  String toString() {
    return 'UserSessionState.authentified(currentUser: $currentUser)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Authentified &&
            (identical(other.currentUser, currentUser) ||
                const DeepCollectionEquality()
                    .equals(other.currentUser, currentUser)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(currentUser);

  @JsonKey(ignore: true)
  @override
  _$AuthentifiedCopyWith<_Authentified> get copyWith =>
      __$AuthentifiedCopyWithImpl<_Authentified>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(User currentUser) authentified,
    required TResult Function() unauthentified,
  }) {
    return authentified(currentUser);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(User currentUser)? authentified,
    TResult Function()? unauthentified,
    required TResult orElse(),
  }) {
    if (authentified != null) {
      return authentified(currentUser);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Authentified value) authentified,
    required TResult Function(_Unauthentified value) unauthentified,
  }) {
    return authentified(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Authentified value)? authentified,
    TResult Function(_Unauthentified value)? unauthentified,
    required TResult orElse(),
  }) {
    if (authentified != null) {
      return authentified(this);
    }
    return orElse();
  }
}

abstract class _Authentified implements UserSessionState {
  const factory _Authentified(User currentUser) = _$_Authentified;

  User get currentUser => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$AuthentifiedCopyWith<_Authentified> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$UnauthentifiedCopyWith<$Res> {
  factory _$UnauthentifiedCopyWith(
          _Unauthentified value, $Res Function(_Unauthentified) then) =
      __$UnauthentifiedCopyWithImpl<$Res>;
}

/// @nodoc
class __$UnauthentifiedCopyWithImpl<$Res>
    extends _$UserSessionStateCopyWithImpl<$Res>
    implements _$UnauthentifiedCopyWith<$Res> {
  __$UnauthentifiedCopyWithImpl(
      _Unauthentified _value, $Res Function(_Unauthentified) _then)
      : super(_value, (v) => _then(v as _Unauthentified));

  @override
  _Unauthentified get _value => super._value as _Unauthentified;
}

/// @nodoc

class _$_Unauthentified implements _Unauthentified {
  const _$_Unauthentified();

  @override
  String toString() {
    return 'UserSessionState.unauthentified()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Unauthentified);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(User currentUser) authentified,
    required TResult Function() unauthentified,
  }) {
    return unauthentified();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(User currentUser)? authentified,
    TResult Function()? unauthentified,
    required TResult orElse(),
  }) {
    if (unauthentified != null) {
      return unauthentified();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Authentified value) authentified,
    required TResult Function(_Unauthentified value) unauthentified,
  }) {
    return unauthentified(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Authentified value)? authentified,
    TResult Function(_Unauthentified value)? unauthentified,
    required TResult orElse(),
  }) {
    if (unauthentified != null) {
      return unauthentified(this);
    }
    return orElse();
  }
}

abstract class _Unauthentified implements UserSessionState {
  const factory _Unauthentified() = _$_Unauthentified;
}
