import 'package:intl/intl.dart';

String formatDate(DateTime date, String locale) {
  return DateFormat("E d MMM", locale).format(date);
}

String formatShortDate(DateTime date, String locale) {
  return DateFormat("d/MM/y", locale).format(date);
}

DateTime minimalDateForNewPoll() {
  DateTime _currentDateTime = DateTime.now();
  if (_currentDateTime.hour > 11 && _currentDateTime.minute > 00) {
    return DateTime(_currentDateTime.year, _currentDateTime.month,
            _currentDateTime.day, 0, 0, 0)
        .add(Duration(days: 1));
  } else {
    return DateTime(_currentDateTime.year, _currentDateTime.month,
        _currentDateTime.day, 0, 0, 0);
  }
}
