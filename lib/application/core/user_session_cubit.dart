import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';

import '../../domain/auth/i_auth_facade.dart';
import '../../domain/core/user.dart';
import 'user_session_state.dart';

@lazySingleton
class UserSessionCubit extends Cubit<UserSessionState> {
  final IAuthFacade _authFacade;
  UserSessionCubit(this._authFacade) : super(UserSessionState.initial());

  void updateCurrentUser(User user) {
    emit(UserSessionState.authentified(user));
  }

  void autoLogin() async {
    // TODO: Delete me, is for fake delay
    await Future.delayed(Duration(seconds: 2));
    final failOrLoggedIn = await _authFacade.autoLogin();
    failOrLoggedIn.fold(
      (fail) => emit(UserSessionState.unauthentified()),
      (user) => emit(UserSessionState.authentified(user)),
    );
  }

  void logout() async {
    await _authFacade.logout();
    emit(UserSessionState.unauthentified());
  }
}
