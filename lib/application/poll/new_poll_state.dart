import 'package:freezed_annotation/freezed_annotation.dart';

import '../../domain/core/restaurant.dart';
import '../../domain/poll/failures.dart';

part 'new_poll_state.freezed.dart';

@freezed
class NewPollState with _$NewPollState {
  const factory NewPollState({
    @Default([]) List<Restaurant> restaurants,
    PollFailure? fail,
    @Default(false) bool submitting,
    @Default(false) bool loading,
    @Default(false) bool loaded,
    @Default(false) bool onError,
    @Default(false) bool created,
  }) = _NewPollState;

  factory NewPollState.initial() => NewPollState();
  factory NewPollState.loaded({required List<Restaurant> restaurants}) =>
      NewPollState(
        restaurants: restaurants,
        loaded: true,
      );

  factory NewPollState.error(PollFailure fail) =>
      NewPollState(fail: fail, onError: true);
}
