import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import '../core/injection.dart';
import '../core/user_session_cubit.dart';
import '../../domain/core/user.dart';
import '../../domain/core/poll.dart';
import '../../domain/core/restaurant.dart';
import '../../domain/poll/failures.dart';
import '../../domain/poll/i_poll_facade.dart';
import '../../domain/poll/i_restaurant_facade.dart';
import 'package:uuid/uuid.dart';

import 'new_poll_state.dart';

@injectable
class NewPollCubit extends Cubit<NewPollState> {
  final IRestaurantFacade _restaurantFacade;
  final IPollFacade _pollFacade;
  NewPollCubit(this._restaurantFacade, this._pollFacade)
      : super(NewPollState.initial()) {
    load();
  }

  void load() async {
    emit(state.copyWith(loading: true));
    final failOrRestaurants = await _restaurantFacade.getAll();
    failOrRestaurants.fold(
      (fail) => emit(NewPollState.error(PollFailure.unknown(fail))),
      (restaurants) => emit(
        NewPollState.loaded(
          restaurants: restaurants,
        ),
      ),
    );
  }

  void create(
      {required DateTime day, required List<Restaurant> choices}) async {
    emit(state.copyWith(submitting: true));
    getIt<UserSessionCubit>().state.maybeMap(
      orElse: () {
        emit(NewPollState.error(PollFailure.unknown("No user authentified")));
      },
      authentified: (authentifiedState) async {
        await _createPoll(authentifiedState.currentUser, day, choices);
      },
    );
  }

  Future<void> _createPoll(
      User creator, DateTime day, List<Restaurant> choices) async {
    final failOrCreated = await _pollFacade.addPoll(
        Poll(id: Uuid().v1(), day: day, choices: choices, creator: creator));
    failOrCreated.fold(
      (fail) => emit(
        state.copyWith(
            loading: false,
            loaded: true,
            submitting: false,
            onError: true,
            fail: fail),
      ),
      (_) => emit(
        state.copyWith(
            submitting: false, created: true, onError: false, fail: null),
      ),
    );
  }
}
