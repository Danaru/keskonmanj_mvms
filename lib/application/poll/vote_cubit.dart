import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import '../core/injection.dart';
import '../core/user_session_cubit.dart';
import '../../domain/core/poll.dart';
import '../../domain/core/restaurant.dart';
import '../../domain/core/user.dart';
import '../../domain/poll/failures.dart';
import '../../domain/poll/i_poll_facade.dart';
import '../../domain/poll/vote.dart';

import 'vote_state.dart';

@injectable
class VoteCubit extends Cubit<VoteState> {
  final IPollFacade _pollFacade;
  VoteCubit(this._pollFacade) : super(VoteState.initial());

  void load(String pollID) async {
    final failOrPoll = await _pollFacade.getPoll(pollID);
    failOrPoll.fold(
      (fail) => emit(VoteState.error(fail: fail, pollID: pollID)),
      (poll) => emit(VoteState.loaded(
          poll: poll,
          userCanVote: canVote(poll),
          isAdmin: _currentUserIsAdmin())),
    );
  }

  bool canVote(Poll poll) {
    User? user = _checkCurrentUser();
    if (user != null) {
      return _userCanVoteForPoll(user, poll);
    } else {
      return false;
    }
  }

  bool _currentUserIsAdmin() {
    User? user = _checkCurrentUser();
    if (user != null && user.username == "test") {
      return true;
    } else {
      return false;
    }
  }

  bool _userCanVoteForPoll(User user, Poll poll) {
    if (!poll.isActive) return false;

    if (poll.votes != null &&
        poll.votes!
            .where((element) => element.votantId == user.username)
            .isNotEmpty) {
      return false;
    }

    return true;
  }

  void vote(Restaurant selectedRestaurant) async {
    User? user = _checkCurrentUser();
    if (user != null) {
      state.maybeMap(
        loaded: (loadedState) async {
          final failOrPoll = await _pollFacade.vote(loadedState.poll,
              Vote(choice: selectedRestaurant, votantId: user.username));
          failOrPoll.fold(
            (fail) => emit(VoteState.error(fail: fail)),
            (poll) => emit(VoteState.loaded(
                poll: poll,
                userCanVote: canVote(poll),
                isAdmin: _currentUserIsAdmin())),
          );
        },
        orElse: () => emit(
            VoteState.error(fail: PollFailure.unknown("Not in good state"))),
      );
    }
  }

  User? _checkCurrentUser() {
    return getIt<UserSessionCubit>().state.maybeMap(
        authentified: (authState) => authState.currentUser,
        orElse: () {
          emit(VoteState.error(
              fail: PollFailure.unknown("Utilisateur non authentifié")));
          return null;
        });
  }

  void generateAndClose() async {
    state.maybeMap(
        loaded: (loadedState) async {
          final closedPoll = await _pollFacade.closePoll(loadedState.poll);
          emit(VoteState.loaded(
              poll: closedPoll, userCanVote: false, isAdmin: false));
        },
        orElse: () {});
  }
}
