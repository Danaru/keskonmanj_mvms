import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../domain/core/poll.dart';
import '../../domain/poll/failures.dart';

part 'vote_state.freezed.dart';

@freezed
class VoteState with _$VoteState {
  const factory VoteState.initial() = _Initial;
  const factory VoteState.loaded(
      {required Poll poll,
      required bool userCanVote,
      required bool isAdmin}) = _Loaded;
  const factory VoteState.error({required PollFailure fail, String? pollID}) =
      _Error;
}
