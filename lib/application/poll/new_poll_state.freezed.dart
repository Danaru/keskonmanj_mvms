// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'new_poll_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$NewPollStateTearOff {
  const _$NewPollStateTearOff();

  _NewPollState call(
      {List<Restaurant> restaurants = const [],
      PollFailure? fail,
      bool submitting = false,
      bool loading = false,
      bool loaded = false,
      bool onError = false,
      bool created = false}) {
    return _NewPollState(
      restaurants: restaurants,
      fail: fail,
      submitting: submitting,
      loading: loading,
      loaded: loaded,
      onError: onError,
      created: created,
    );
  }
}

/// @nodoc
const $NewPollState = _$NewPollStateTearOff();

/// @nodoc
mixin _$NewPollState {
  List<Restaurant> get restaurants => throw _privateConstructorUsedError;
  PollFailure? get fail => throw _privateConstructorUsedError;
  bool get submitting => throw _privateConstructorUsedError;
  bool get loading => throw _privateConstructorUsedError;
  bool get loaded => throw _privateConstructorUsedError;
  bool get onError => throw _privateConstructorUsedError;
  bool get created => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $NewPollStateCopyWith<NewPollState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NewPollStateCopyWith<$Res> {
  factory $NewPollStateCopyWith(
          NewPollState value, $Res Function(NewPollState) then) =
      _$NewPollStateCopyWithImpl<$Res>;
  $Res call(
      {List<Restaurant> restaurants,
      PollFailure? fail,
      bool submitting,
      bool loading,
      bool loaded,
      bool onError,
      bool created});

  $PollFailureCopyWith<$Res>? get fail;
}

/// @nodoc
class _$NewPollStateCopyWithImpl<$Res> implements $NewPollStateCopyWith<$Res> {
  _$NewPollStateCopyWithImpl(this._value, this._then);

  final NewPollState _value;
  // ignore: unused_field
  final $Res Function(NewPollState) _then;

  @override
  $Res call({
    Object? restaurants = freezed,
    Object? fail = freezed,
    Object? submitting = freezed,
    Object? loading = freezed,
    Object? loaded = freezed,
    Object? onError = freezed,
    Object? created = freezed,
  }) {
    return _then(_value.copyWith(
      restaurants: restaurants == freezed
          ? _value.restaurants
          : restaurants // ignore: cast_nullable_to_non_nullable
              as List<Restaurant>,
      fail: fail == freezed
          ? _value.fail
          : fail // ignore: cast_nullable_to_non_nullable
              as PollFailure?,
      submitting: submitting == freezed
          ? _value.submitting
          : submitting // ignore: cast_nullable_to_non_nullable
              as bool,
      loading: loading == freezed
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      loaded: loaded == freezed
          ? _value.loaded
          : loaded // ignore: cast_nullable_to_non_nullable
              as bool,
      onError: onError == freezed
          ? _value.onError
          : onError // ignore: cast_nullable_to_non_nullable
              as bool,
      created: created == freezed
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }

  @override
  $PollFailureCopyWith<$Res>? get fail {
    if (_value.fail == null) {
      return null;
    }

    return $PollFailureCopyWith<$Res>(_value.fail!, (value) {
      return _then(_value.copyWith(fail: value));
    });
  }
}

/// @nodoc
abstract class _$NewPollStateCopyWith<$Res>
    implements $NewPollStateCopyWith<$Res> {
  factory _$NewPollStateCopyWith(
          _NewPollState value, $Res Function(_NewPollState) then) =
      __$NewPollStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {List<Restaurant> restaurants,
      PollFailure? fail,
      bool submitting,
      bool loading,
      bool loaded,
      bool onError,
      bool created});

  @override
  $PollFailureCopyWith<$Res>? get fail;
}

/// @nodoc
class __$NewPollStateCopyWithImpl<$Res> extends _$NewPollStateCopyWithImpl<$Res>
    implements _$NewPollStateCopyWith<$Res> {
  __$NewPollStateCopyWithImpl(
      _NewPollState _value, $Res Function(_NewPollState) _then)
      : super(_value, (v) => _then(v as _NewPollState));

  @override
  _NewPollState get _value => super._value as _NewPollState;

  @override
  $Res call({
    Object? restaurants = freezed,
    Object? fail = freezed,
    Object? submitting = freezed,
    Object? loading = freezed,
    Object? loaded = freezed,
    Object? onError = freezed,
    Object? created = freezed,
  }) {
    return _then(_NewPollState(
      restaurants: restaurants == freezed
          ? _value.restaurants
          : restaurants // ignore: cast_nullable_to_non_nullable
              as List<Restaurant>,
      fail: fail == freezed
          ? _value.fail
          : fail // ignore: cast_nullable_to_non_nullable
              as PollFailure?,
      submitting: submitting == freezed
          ? _value.submitting
          : submitting // ignore: cast_nullable_to_non_nullable
              as bool,
      loading: loading == freezed
          ? _value.loading
          : loading // ignore: cast_nullable_to_non_nullable
              as bool,
      loaded: loaded == freezed
          ? _value.loaded
          : loaded // ignore: cast_nullable_to_non_nullable
              as bool,
      onError: onError == freezed
          ? _value.onError
          : onError // ignore: cast_nullable_to_non_nullable
              as bool,
      created: created == freezed
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_NewPollState implements _NewPollState {
  const _$_NewPollState(
      {this.restaurants = const [],
      this.fail,
      this.submitting = false,
      this.loading = false,
      this.loaded = false,
      this.onError = false,
      this.created = false});

  @JsonKey(defaultValue: const [])
  @override
  final List<Restaurant> restaurants;
  @override
  final PollFailure? fail;
  @JsonKey(defaultValue: false)
  @override
  final bool submitting;
  @JsonKey(defaultValue: false)
  @override
  final bool loading;
  @JsonKey(defaultValue: false)
  @override
  final bool loaded;
  @JsonKey(defaultValue: false)
  @override
  final bool onError;
  @JsonKey(defaultValue: false)
  @override
  final bool created;

  @override
  String toString() {
    return 'NewPollState(restaurants: $restaurants, fail: $fail, submitting: $submitting, loading: $loading, loaded: $loaded, onError: $onError, created: $created)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _NewPollState &&
            (identical(other.restaurants, restaurants) ||
                const DeepCollectionEquality()
                    .equals(other.restaurants, restaurants)) &&
            (identical(other.fail, fail) ||
                const DeepCollectionEquality().equals(other.fail, fail)) &&
            (identical(other.submitting, submitting) ||
                const DeepCollectionEquality()
                    .equals(other.submitting, submitting)) &&
            (identical(other.loading, loading) ||
                const DeepCollectionEquality()
                    .equals(other.loading, loading)) &&
            (identical(other.loaded, loaded) ||
                const DeepCollectionEquality().equals(other.loaded, loaded)) &&
            (identical(other.onError, onError) ||
                const DeepCollectionEquality()
                    .equals(other.onError, onError)) &&
            (identical(other.created, created) ||
                const DeepCollectionEquality().equals(other.created, created)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(restaurants) ^
      const DeepCollectionEquality().hash(fail) ^
      const DeepCollectionEquality().hash(submitting) ^
      const DeepCollectionEquality().hash(loading) ^
      const DeepCollectionEquality().hash(loaded) ^
      const DeepCollectionEquality().hash(onError) ^
      const DeepCollectionEquality().hash(created);

  @JsonKey(ignore: true)
  @override
  _$NewPollStateCopyWith<_NewPollState> get copyWith =>
      __$NewPollStateCopyWithImpl<_NewPollState>(this, _$identity);
}

abstract class _NewPollState implements NewPollState {
  const factory _NewPollState(
      {List<Restaurant> restaurants,
      PollFailure? fail,
      bool submitting,
      bool loading,
      bool loaded,
      bool onError,
      bool created}) = _$_NewPollState;

  @override
  List<Restaurant> get restaurants => throw _privateConstructorUsedError;
  @override
  PollFailure? get fail => throw _privateConstructorUsedError;
  @override
  bool get submitting => throw _privateConstructorUsedError;
  @override
  bool get loading => throw _privateConstructorUsedError;
  @override
  bool get loaded => throw _privateConstructorUsedError;
  @override
  bool get onError => throw _privateConstructorUsedError;
  @override
  bool get created => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$NewPollStateCopyWith<_NewPollState> get copyWith =>
      throw _privateConstructorUsedError;
}
