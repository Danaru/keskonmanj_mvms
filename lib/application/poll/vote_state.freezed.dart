// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'vote_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$VoteStateTearOff {
  const _$VoteStateTearOff();

  _Initial initial() {
    return const _Initial();
  }

  _Loaded loaded(
      {required Poll poll, required bool userCanVote, required bool isAdmin}) {
    return _Loaded(
      poll: poll,
      userCanVote: userCanVote,
      isAdmin: isAdmin,
    );
  }

  _Error error({required PollFailure fail, String? pollID}) {
    return _Error(
      fail: fail,
      pollID: pollID,
    );
  }
}

/// @nodoc
const $VoteState = _$VoteStateTearOff();

/// @nodoc
mixin _$VoteState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Poll poll, bool userCanVote, bool isAdmin) loaded,
    required TResult Function(PollFailure fail, String? pollID) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Poll poll, bool userCanVote, bool isAdmin)? loaded,
    TResult Function(PollFailure fail, String? pollID)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VoteStateCopyWith<$Res> {
  factory $VoteStateCopyWith(VoteState value, $Res Function(VoteState) then) =
      _$VoteStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$VoteStateCopyWithImpl<$Res> implements $VoteStateCopyWith<$Res> {
  _$VoteStateCopyWithImpl(this._value, this._then);

  final VoteState _value;
  // ignore: unused_field
  final $Res Function(VoteState) _then;
}

/// @nodoc
abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$InitialCopyWithImpl<$Res> extends _$VoteStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

/// @nodoc

class _$_Initial with DiagnosticableTreeMixin implements _Initial {
  const _$_Initial();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'VoteState.initial()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'VoteState.initial'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Poll poll, bool userCanVote, bool isAdmin) loaded,
    required TResult Function(PollFailure fail, String? pollID) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Poll poll, bool userCanVote, bool isAdmin)? loaded,
    TResult Function(PollFailure fail, String? pollID)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements VoteState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$LoadedCopyWith<$Res> {
  factory _$LoadedCopyWith(_Loaded value, $Res Function(_Loaded) then) =
      __$LoadedCopyWithImpl<$Res>;
  $Res call({Poll poll, bool userCanVote, bool isAdmin});

  $PollCopyWith<$Res> get poll;
}

/// @nodoc
class __$LoadedCopyWithImpl<$Res> extends _$VoteStateCopyWithImpl<$Res>
    implements _$LoadedCopyWith<$Res> {
  __$LoadedCopyWithImpl(_Loaded _value, $Res Function(_Loaded) _then)
      : super(_value, (v) => _then(v as _Loaded));

  @override
  _Loaded get _value => super._value as _Loaded;

  @override
  $Res call({
    Object? poll = freezed,
    Object? userCanVote = freezed,
    Object? isAdmin = freezed,
  }) {
    return _then(_Loaded(
      poll: poll == freezed
          ? _value.poll
          : poll // ignore: cast_nullable_to_non_nullable
              as Poll,
      userCanVote: userCanVote == freezed
          ? _value.userCanVote
          : userCanVote // ignore: cast_nullable_to_non_nullable
              as bool,
      isAdmin: isAdmin == freezed
          ? _value.isAdmin
          : isAdmin // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }

  @override
  $PollCopyWith<$Res> get poll {
    return $PollCopyWith<$Res>(_value.poll, (value) {
      return _then(_value.copyWith(poll: value));
    });
  }
}

/// @nodoc

class _$_Loaded with DiagnosticableTreeMixin implements _Loaded {
  const _$_Loaded(
      {required this.poll, required this.userCanVote, required this.isAdmin});

  @override
  final Poll poll;
  @override
  final bool userCanVote;
  @override
  final bool isAdmin;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'VoteState.loaded(poll: $poll, userCanVote: $userCanVote, isAdmin: $isAdmin)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'VoteState.loaded'))
      ..add(DiagnosticsProperty('poll', poll))
      ..add(DiagnosticsProperty('userCanVote', userCanVote))
      ..add(DiagnosticsProperty('isAdmin', isAdmin));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Loaded &&
            (identical(other.poll, poll) ||
                const DeepCollectionEquality().equals(other.poll, poll)) &&
            (identical(other.userCanVote, userCanVote) ||
                const DeepCollectionEquality()
                    .equals(other.userCanVote, userCanVote)) &&
            (identical(other.isAdmin, isAdmin) ||
                const DeepCollectionEquality().equals(other.isAdmin, isAdmin)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(poll) ^
      const DeepCollectionEquality().hash(userCanVote) ^
      const DeepCollectionEquality().hash(isAdmin);

  @JsonKey(ignore: true)
  @override
  _$LoadedCopyWith<_Loaded> get copyWith =>
      __$LoadedCopyWithImpl<_Loaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Poll poll, bool userCanVote, bool isAdmin) loaded,
    required TResult Function(PollFailure fail, String? pollID) error,
  }) {
    return loaded(poll, userCanVote, isAdmin);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Poll poll, bool userCanVote, bool isAdmin)? loaded,
    TResult Function(PollFailure fail, String? pollID)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(poll, userCanVote, isAdmin);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements VoteState {
  const factory _Loaded(
      {required Poll poll,
      required bool userCanVote,
      required bool isAdmin}) = _$_Loaded;

  Poll get poll => throw _privateConstructorUsedError;
  bool get userCanVote => throw _privateConstructorUsedError;
  bool get isAdmin => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$LoadedCopyWith<_Loaded> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$ErrorCopyWith<$Res> {
  factory _$ErrorCopyWith(_Error value, $Res Function(_Error) then) =
      __$ErrorCopyWithImpl<$Res>;
  $Res call({PollFailure fail, String? pollID});

  $PollFailureCopyWith<$Res> get fail;
}

/// @nodoc
class __$ErrorCopyWithImpl<$Res> extends _$VoteStateCopyWithImpl<$Res>
    implements _$ErrorCopyWith<$Res> {
  __$ErrorCopyWithImpl(_Error _value, $Res Function(_Error) _then)
      : super(_value, (v) => _then(v as _Error));

  @override
  _Error get _value => super._value as _Error;

  @override
  $Res call({
    Object? fail = freezed,
    Object? pollID = freezed,
  }) {
    return _then(_Error(
      fail: fail == freezed
          ? _value.fail
          : fail // ignore: cast_nullable_to_non_nullable
              as PollFailure,
      pollID: pollID == freezed
          ? _value.pollID
          : pollID // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $PollFailureCopyWith<$Res> get fail {
    return $PollFailureCopyWith<$Res>(_value.fail, (value) {
      return _then(_value.copyWith(fail: value));
    });
  }
}

/// @nodoc

class _$_Error with DiagnosticableTreeMixin implements _Error {
  const _$_Error({required this.fail, this.pollID});

  @override
  final PollFailure fail;
  @override
  final String? pollID;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'VoteState.error(fail: $fail, pollID: $pollID)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'VoteState.error'))
      ..add(DiagnosticsProperty('fail', fail))
      ..add(DiagnosticsProperty('pollID', pollID));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Error &&
            (identical(other.fail, fail) ||
                const DeepCollectionEquality().equals(other.fail, fail)) &&
            (identical(other.pollID, pollID) ||
                const DeepCollectionEquality().equals(other.pollID, pollID)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(fail) ^
      const DeepCollectionEquality().hash(pollID);

  @JsonKey(ignore: true)
  @override
  _$ErrorCopyWith<_Error> get copyWith =>
      __$ErrorCopyWithImpl<_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(Poll poll, bool userCanVote, bool isAdmin) loaded,
    required TResult Function(PollFailure fail, String? pollID) error,
  }) {
    return error(fail, pollID);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(Poll poll, bool userCanVote, bool isAdmin)? loaded,
    TResult Function(PollFailure fail, String? pollID)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(fail, pollID);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements VoteState {
  const factory _Error({required PollFailure fail, String? pollID}) = _$_Error;

  PollFailure get fail => throw _privateConstructorUsedError;
  String? get pollID => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$ErrorCopyWith<_Error> get copyWith => throw _privateConstructorUsedError;
}
