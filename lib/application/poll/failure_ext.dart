import '../../domain/poll/failures.dart';

extension PollFailureExt on PollFailure {
  String get message => this.map(
        unknown: (fail) => fail.internalError.toString(),
        yetExistForThisDay: (fail) =>
            "Un sondage existe déjà pour le jour que vous avez choisi",
        notFound: (_) => "Ce sondage n'existe pas",
      );
}
