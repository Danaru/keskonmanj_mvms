import 'package:flutter/material.dart';

import 'application/core/injection.dart';
import 'presentation/core/router.gr.dart';

void main() {
  configureDependencies();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _approuter = KKMRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
        title: 'KesKonManj',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routeInformationParser: _approuter.defaultRouteParser(),
        routerDelegate: _approuter.delegate());
  }
}
