import 'package:injectable/injectable.dart';

import '../../domain/core/user.dart';
import 'i_user_data_source.dart';

@LazySingleton(as: IUserRemoteDataSource)
class FakeUserRemoteDataSource implements IUserRemoteDataSource {
  final _fakeUsers = [
    User(email: "test@test.fr", username: "test", password: "testpass"),
    User(email: "test2@test.fr", username: "test2", password: "test2pass"),
  ];

  Future<User> createAccount(
      {required String username,
      required String password,
      required String email}) async {
    await Future.delayed(Duration(seconds: 3));
    if (_fakeUsers
        .where((element) =>
            element.username == username &&
            element.password == password &&
            element.email == email)
        .isEmpty) {
      return User(email: email, username: username, password: password);
    }

    throw NullThrownError();
  }

  Future<User> signIn({
    required String username,
    required String password,
  }) async {
    await Future.delayed(Duration(seconds: 3));
    final knownUser = _fakeUsers.singleWhere((element) =>
        element.username == username && element.password == password);
    return knownUser.copyWith(password: password);
  }
}
