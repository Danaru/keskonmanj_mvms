import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

import '../../domain/core/user.dart';

@lazySingleton
class CredentialsLocalDataSource {
  final storage = FlutterSecureStorage();

  Future<void> saveUser(User user) async {
    await storage.write(key: "_currentUser", value: jsonEncode(user.toJson()));
  }

  Future<User> getUser() async {
    final rawData = await storage.read(key: "_currentUser");
    return User.fromJson(jsonDecode(rawData!));
  }

  Future destroy() async {
    await storage.delete(key: "_currentUser");
  }
}
