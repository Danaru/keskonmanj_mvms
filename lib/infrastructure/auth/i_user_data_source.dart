import '../../domain/core/user.dart';

abstract class IUserRemoteDataSource {
  Future<User> createAccount(
      {required String username,
      required String password,
      required String email});

  Future<User> signIn({
    required String username,
    required String password,
  });
}
