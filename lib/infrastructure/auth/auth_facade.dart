import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../domain/auth/failures.dart';
import '../../domain/auth/i_auth_facade.dart';
import '../../domain/core/user.dart';
import 'credentials_local_data_source.dart';
import 'i_user_data_source.dart';

@LazySingleton(as: IAuthFacade)
class AuthFacade implements IAuthFacade {
  final IUserRemoteDataSource _userDataSource;
  final CredentialsLocalDataSource _credentialsLocalDataSource;

  AuthFacade(this._userDataSource, this._credentialsLocalDataSource);

  @override
  Future<Either<RegisterAccountFailure, User>> registerAccount({
    required String email,
    required String username,
    required String password,
  }) async {
    try {
      final createdUser = await _userDataSource.createAccount(
          username: username, password: password, email: email);
      return right(createdUser);
    } catch (ex) {
      return left(
          RegisterAccountFailure.unknown("Oops, a strange error occured"));
    }
  }

  @override
  Future<Either<SignInFailure, User>> signIn(
      {required String username, required String password}) async {
    await Future.delayed(Duration(seconds: 3));
    try {
      final loggedUser =
          await _userDataSource.signIn(username: username, password: password);
      await _credentialsLocalDataSource.saveUser(loggedUser);
      return right(loggedUser);
    } catch (ex) {
      return left(SignInFailure.badCredentials());
    }
  }

  @override
  Future<Either<SignInFailure, User>> autoLogin() async {
    try {
      final savedUser = await _credentialsLocalDataSource.getUser();
      return await signIn(
          username: savedUser.username, password: savedUser.password!);
    } catch (ex) {
      return left(SignInFailure.unknown(ex));
    }
  }

  @override
  Future<void> logout() async {
    await _credentialsLocalDataSource.destroy();
  }
}
