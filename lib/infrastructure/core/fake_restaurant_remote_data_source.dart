import 'package:injectable/injectable.dart';

import '../../domain/core/restaurant.dart';
import 'i_restaurant_remote_data_source.dart';

@LazySingleton(as: IRestaurantRemoteDataSource)
class FakeRestaurantRemoteDataSource implements IRestaurantRemoteDataSource {
  final List<Restaurant> restaurants = [
    Restaurant(name: "Pizza"),
    Restaurant(name: "Jap"),
    Restaurant(name: "Burger"),
    Restaurant(name: "Salade"),
    Restaurant(name: "Viet"),
    Restaurant(name: "Tacos"),
    Restaurant(name: "Français"),
  ];
  @override
  Future<List<Restaurant>> getAll() async {
    return restaurants;
  }
}
