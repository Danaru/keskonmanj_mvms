import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import '../../domain/poll/failures.dart';
import '../../domain/core/restaurant.dart';
import '../../domain/poll/i_restaurant_facade.dart';

import 'i_restaurant_remote_data_source.dart';

@LazySingleton(as: IRestaurantFacade)
class RestaurantFacade implements IRestaurantFacade {
  final IRestaurantRemoteDataSource _restaurantRemoteDataSource;

  RestaurantFacade(this._restaurantRemoteDataSource);
  @override
  Future<Either<RestaurantFailure, List<Restaurant>>> getAll() async {
    try {
      final restaurants = await _restaurantRemoteDataSource.getAll();
      return right(restaurants);
    } catch (ex) {
      return left(RestaurantFailure.unknown(ex));
    }
  }
}
