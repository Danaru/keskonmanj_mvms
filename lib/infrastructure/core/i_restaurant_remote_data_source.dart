import '../../domain/core/restaurant.dart';

abstract class IRestaurantRemoteDataSource {
  Future<List<Restaurant>> getAll();
}
