import '../../domain/poll/vote.dart';

import '../../domain/core/poll.dart';

abstract class IPollRemoteDataSource {
  Future<List<Poll>> getAll();

  Future<List<Poll>> add(Poll newPoll);

  Future<Poll> get(String pollID);
  Future<Poll> update(Poll poll);
  Future<Poll> addVote(String pollID, Vote vote);
}
