import 'package:injectable/injectable.dart';
import 'fake_poll_list.dart';
import '../../domain/poll/vote.dart';
import 'exceptions.dart';
import '../../domain/core/poll.dart';
import 'i_poll_remote_data_source.dart';

@LazySingleton(as: IPollRemoteDataSource)
class FakePollRemoteDataSource implements IPollRemoteDataSource {
  List<Poll> _polls = fakePolls;

  @override
  Future<List<Poll>> getAll() async {
    _sortPolls();
    return _polls;
  }

  @override
  Future<List<Poll>> add(Poll newPoll) async {
    if (pollAlreadyExistsForThisDay(newPoll.day)) {
      throw YetSetFoThisDay(newPoll.day);
    } else {
      _polls.add(newPoll);
      _sortPolls();
      return _polls;
    }
  }

  void _sortPolls() {
    _polls.sort((a, b) => b.day.compareTo(a.day));
  }

  bool pollAlreadyExistsForThisDay(DateTime day) {
    final referenceDay = DateTime(day.year, day.month, day.day);
    final existingDates =
        _polls.map((e) => DateTime(e.day.year, e.day.month, e.day.day));

    return existingDates.contains(referenceDay);
  }

  @override
  Future<Poll> get(String pollID) async {
    return _polls.singleWhere((poll) => poll.id == pollID);
  }

  @override
  Future<Poll> addVote(String pollID, Vote vote) async {
    final poll = await get(pollID);
    var updatedPoll;
    if (poll.votes == null) {
      updatedPoll = poll.copyWith(votes: [vote]);
    } else if (poll.votes!
        .where((element) => element.votantId == vote.votantId)
        .isEmpty) {
      poll.votes!.add(vote);
      updatedPoll = poll;
    }
    return await update(updatedPoll);
  }

  @override
  Future<Poll> update(Poll poll) async {
    _polls.removeWhere((_poll) => _poll.id == poll.id);
    _polls.add(poll);
    return poll;
  }
}
