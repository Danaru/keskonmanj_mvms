import '../../domain/core/poll.dart';
import '../../domain/core/restaurant.dart';
import '../../domain/core/user.dart';
import '../../domain/poll/vote.dart';
import 'package:uuid/uuid.dart';

List<Poll> fakePolls = [
  Poll(
    id: Uuid().v1(),
    day: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
        .add(Duration(days: -4)),
    choices: [
      Restaurant(name: "Pizza"),
      Restaurant(name: "Viet"),
      Restaurant(name: "Salade")
    ],
    votes: [
      Vote(choice: Restaurant(name: "Pizza"), votantId: 'test2'),
      Vote(choice: Restaurant(name: "Pizza"), votantId: 'test3'),
      Vote(choice: Restaurant(name: "Pizza"), votantId: 'test4')
    ],
    closed: true,
    creator: User(email: "test@test.fr", username: "test"),
    results: [VoteResult(Restaurant(name: "Pizza"), 3)],
  ),
  Poll(
    id: Uuid().v1(),
    day: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
        .add(Duration(days: -3)),
    choices: [
      Restaurant(name: "Salade"),
      Restaurant(name: "Burger"),
      Restaurant(name: "Jap")
    ],
    closed: true,
    creator: User(email: "test@test.fr", username: "test"),
    votes: [
      Vote(choice: Restaurant(name: "Salade"), votantId: 'test2'),
      Vote(choice: Restaurant(name: "Salade"), votantId: 'test3'),
      Vote(choice: Restaurant(name: "Burger"), votantId: 'test4')
    ],
    results: [
      VoteResult(Restaurant(name: "Salade"), 2),
      VoteResult(Restaurant(name: "Burger"), 1)
    ],
  ),
  Poll(
    id: Uuid().v1(),
    day: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
        .add(Duration(days: -2)),
    choices: [
      Restaurant(name: "Pizza"),
      Restaurant(name: "Tacos"),
      Restaurant(name: "Français")
    ],
    closed: true,
    creator: User(email: "test@test.fr", username: "test"),
    votes: [
      Vote(choice: Restaurant(name: "Tacos"), votantId: 'test2'),
      Vote(choice: Restaurant(name: "Français"), votantId: 'test3'),
      Vote(choice: Restaurant(name: "Tacos"), votantId: 'test4')
    ],
    results: [
      VoteResult(Restaurant(name: "Tacos"), 2),
      VoteResult(Restaurant(name: "Français"), 1)
    ],
  ),
  Poll(
    id: Uuid().v1(),
    day: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
        .add(Duration(days: -1)),
    choices: [
      Restaurant(name: "Pizza"),
      Restaurant(name: "Burger"),
      Restaurant(name: "Jap")
    ],
    closed: true,
    creator: User(email: "test@test.fr", username: "test"),
    votes: [
      Vote(choice: Restaurant(name: "Jap"), votantId: 'test2'),
      Vote(choice: Restaurant(name: "Jap"), votantId: 'test3'),
      Vote(choice: Restaurant(name: "Jap"), votantId: 'test4')
    ],
    results: [
      VoteResult(Restaurant(name: "Jap"), 3),
    ],
  )
];
