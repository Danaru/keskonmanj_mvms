import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import '../../domain/poll/vote.dart';
import 'exceptions.dart';
import '../../domain/poll/failures.dart';
import '../../domain/core/poll.dart';
import '../../domain/poll/i_poll_facade.dart';

import 'i_poll_remote_data_source.dart';

@LazySingleton(as: IPollFacade)
class PollsFacade implements IPollFacade {
  final StreamController _pollStreamController = StreamController.broadcast();

  final IPollRemoteDataSource _pollRemoteDataSource;

  PollsFacade(this._pollRemoteDataSource);

  @override
  Future<Either<PollFailure, List<Poll>>> addPoll(Poll newPoll) async {
    try {
      final polls = await _pollRemoteDataSource.add(newPoll);
      _pollStreamController.add(null);
      return right(polls);
    } on YetSetFoThisDay catch (ex) {
      return left(PollFailure.yetExistForThisDay(ex.day));
    } catch (ex) {
      return left(PollFailure.unknown(ex));
    }
  }

  @override
  Future<Either<PollFailure, List<Poll>>> getPolls() async {
    try {
      final polls = await _pollRemoteDataSource.getAll();
      return right(polls);
    } catch (ex) {
      return left(PollFailure.unknown(ex));
    }
  }

  @override
  Future<Either<PollFailure, Poll>> getPoll(String pollID) async {
    try {
      final poll = await _pollRemoteDataSource.get(pollID);
      return right(poll);
    } on StateError catch (_) {
      return left(PollFailure.notFound());
    } catch (ex) {
      return left(PollFailure.unknown(ex));
    }
  }

  @override
  Future<Either<PollFailure, Poll>> vote(Poll poll, Vote vote) async {
    try {
      var updatedPoll = await _pollRemoteDataSource.addVote(poll.id, vote);
      updatedPoll = await _calculateResults(updatedPoll);
      await _pollRemoteDataSource.update(updatedPoll);
      _pollStreamController.add(null);
      return right(updatedPoll);
    } catch (ex) {
      return left(PollFailure.unknown(ex));
    }
  }

  Future<Poll> _calculateResults(Poll poll) async {
    if (poll.votes == null) {
      return poll;
    }

    List<VoteResult> results = [];
    poll.votes!.forEach((vote) {
      if (results
          .where((element) => element.restaurant == vote.choice)
          .isEmpty) {
        results.add(VoteResult(vote.choice, 1));
      } else {
        final yetResult =
            results.singleWhere((element) => element.restaurant == vote.choice);

        results.remove(yetResult);
        final newResult = yetResult.copyWith(points: yetResult.points + 1);
        results.add(newResult);
      }
    });

    return poll.copyWith(results: results);
  }

  @override
  Stream get pollStream => _pollStreamController.stream;

  @disposeMethod
  void dispose() {
    _pollStreamController.close();
  }

  @override
  Future<Poll> closePoll(Poll poll) async {
    try {
      var _currentPoll = await _generateFakeVotes(poll, 7);
      final pollWithResult = await _calculateResults(_currentPoll);
      final closedPoll = pollWithResult.copyWith(closed: true);
      await _pollRemoteDataSource.update(closedPoll);
      _pollStreamController.add(null);
      return closedPoll;
    } catch (ex) {
      return poll;
    }
  }

  Future<Poll> _generateFakeVotes(Poll poll, int votesCount) async {
    List<Vote> _generated = List.generate(votesCount, (index) {
      poll.choices.shuffle();
      return Vote(votantId: "test$index", choice: poll.choices.first);
    });
    if (poll.votes != null) {
      final toDelete = _generated
          .where((generatedVote) => poll.votes!
              .where((vote) => vote.votantId == generatedVote.votantId)
              .isNotEmpty)
          .toList();
      toDelete.forEach((voteToDel) {
        _generated.remove(voteToDel);
      });
    }
    return poll.copyWith(votes: _generated);
  }
}
