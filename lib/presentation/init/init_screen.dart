import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../application/core/injection.dart';
import '../../application/core/user_session_cubit.dart';
import '../../application/core/user_session_state.dart';
import '../core/router.gr.dart';

class InitScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<UserSessionCubit, UserSessionState>(
      bloc: getIt<UserSessionCubit>()..autoLogin(),
      listener: (context, state) {
        state.maybeMap(
            authentified: (_) {
              context.router.pushAndPopUntil(DashboardScreenRoute(),
                  predicate: (_) => false);
            },
            unauthentified: (_) {
              context.router.pushAndPopUntil(LandingScreenRoute(),
                  predicate: (_) => false);
            },
            orElse: () {});
      },
      child: Scaffold(
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/ban.jpg"), fit: BoxFit.cover)),
            ),
            Expanded(
                child: Center(
              child: Text("KesKonManj",
                  style: Theme.of(context).textTheme.headline6),
            )),
            Expanded(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
