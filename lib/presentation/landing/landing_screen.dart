import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../core/router.gr.dart';

class LandingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/ban.jpg"), fit: BoxFit.cover)),
        child: Center(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(25.0)),
            width: MediaQuery.of(context).size.width * .5,
            height: MediaQuery.of(context).size.height * .3,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("KesKonManj?",
                    style: Theme.of(context).textTheme.headline6),
                Text("Il est temps de choisir !"),
                ElevatedButton(
                  onPressed: () {
                    context.router.push(RegisterAccountScreenRoute());
                  },
                  child: Text("Incription"),
                  style: ButtonStyle(
                      backgroundColor: MaterialStateColor.resolveWith(
                          (states) => Colors.blue)),
                ),
                ElevatedButton(
                    onPressed: () {
                      context.router.push(SignInScreenRoute());
                    },
                    child: Text("Connexion"),
                    style: ButtonStyle(
                        backgroundColor: MaterialStateColor.resolveWith(
                            (states) => Colors.green)))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
