import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../application/auth/failure_ext.dart';
import '../../application/auth/sign_in_account_cubit.dart';
import '../../application/auth/sign_in_account_state.dart';
import '../../application/core/injection.dart';
import '../../domain/core/validators.dart';
import '../core/form_fields/kkm_text_form_fields.dart';
import '../core/router.gr.dart';
import '../core/widgets/dialogs_widget.dart';

class SignInScreen extends StatelessWidget {
  static const DIALOG_KEY = ValueKey('SIGNIN_DIALOG');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Connexion",
            style: Theme.of(context)
                .textTheme
                .headline6!
                .copyWith(color: Colors.black)),
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: BlocProvider<SignInAccountCubit>(
          create: (context) => getIt<SignInAccountCubit>(),
          child: BlocListener<SignInAccountCubit, SignInAccountState>(
              listener: (context, state) {
                state.maybeMap(
                    signing: (_) {
                      KKMDialog.show(context,
                          KKMDialog.waiting(title: "Connexion en cours"),
                          key: DIALOG_KEY);
                    },
                    failed: (failedState) {
                      KKMDialog.hide(context, DIALOG_KEY);
                      KKMDialog.show(
                          context,
                          KKMDialog.error(
                            errorMessage:
                                failedState.failure.message.toString(),
                            key: DIALOG_KEY,
                          ),
                          key: DIALOG_KEY);
                    },
                    succeed: (_) async {
                      KKMDialog.hide(context, DIALOG_KEY);
                      context.router.pushAndPopUntil(DashboardScreenRoute(),
                          predicate: (route) => false);
                    },
                    orElse: () {});
              },
              child: _SignInForm())),
    );
  }
}

class _SignInForm extends StatefulWidget {
  @override
  __SignInFormState createState() => __SignInFormState();
}

class __SignInFormState extends State<_SignInForm> {
  final _formKey = GlobalKey<FormState>();
  final _usernameCtrl = TextEditingController();
  final _passwordCtrl = TextEditingController();

  @override
  void dispose() {
    _usernameCtrl.dispose();
    _passwordCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              KKMTextFormField(
                controller: _usernameCtrl,
                labelText: "Nom d'utilisateur",
                hintText: "test",
                validator: GenericValidators.mandatory,
              ),
              KKMTextFormField.password(
                controller: _passwordCtrl,
                hintText: "testpass",
                labelText: "Mot de passe",
                validateLength: false,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      context.read<SignInAccountCubit>().signIn(
                          username: _usernameCtrl.text,
                          password: _passwordCtrl.text);
                    }
                  },
                  child: Text("Se connecter"))
            ],
          ),
        ),
      ),
    );
  }
}
