import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../application/auth/failure_ext.dart';
import '../../application/auth/register_account_cubit.dart';
import '../../application/auth/register_account_state.dart';
import '../../application/core/injection.dart';
import '../../domain/core/validators.dart';
import '../core/form_fields/kkm_text_form_fields.dart';
import '../core/router.gr.dart';
import '../core/widgets/dialogs_widget.dart';

class RegisterAccountScreen extends StatelessWidget {
  static const DIALOG_KEY = ValueKey('REGISTER_DIALOG');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Inscription",
            style: Theme.of(context)
                .textTheme
                .headline6!
                .copyWith(color: Colors.black)),
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: BlocProvider<RegisterAccountCubit>(
        create: (context) => getIt<RegisterAccountCubit>(),
        child: BlocListener<RegisterAccountCubit, RegisterAccountState>(
            listener: (context, state) {
              state.maybeMap(
                  registering: (_) {
                    KKMDialog.show(context,
                        KKMDialog.waiting(title: "Inscription en cours"),
                        key: DIALOG_KEY);
                  },
                  failed: (failedState) {
                    KKMDialog.hide(context, DIALOG_KEY);
                    KKMDialog.show(
                        context,
                        KKMDialog.error(
                          errorMessage: failedState.failure.message.toString(),
                          key: DIALOG_KEY,
                        ),
                        key: DIALOG_KEY);
                  },
                  succeed: (_) async {
                    KKMDialog.hide(context, DIALOG_KEY);
                    context.router.pushAndPopUntil(DashboardScreenRoute(),
                        predicate: (route) => false);
                  },
                  orElse: () {});
            },
            child: _RegisterForm()),
      ),
    );
  }
}

class _RegisterForm extends StatefulWidget {
  @override
  __RegisterFormState createState() => __RegisterFormState();
}

class __RegisterFormState extends State<_RegisterForm> {
  final _formKey = GlobalKey<FormState>();

  final _usernameCtrl = TextEditingController();
  final _emailCtrl = TextEditingController();
  final _passwordCtrl = TextEditingController();

  @override
  void dispose() {
    _usernameCtrl.dispose();
    _emailCtrl.dispose();
    _passwordCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              KKMTextFormField(
                controller: _usernameCtrl,
                labelText: "Nom d'utilisateur",
                hintText: "test3",
                validator: GenericValidators.mandatory,
              ),
              KKMTextFormField.email(
                controller: _emailCtrl,
                labelText: "Addresse email",
                hintText: "test3@test.fr",
              ),
              KKMTextFormField.password(
                controller: _passwordCtrl,
                hintText: "test3pass",
                labelText: "Mot de passe",
                validateLength: true,
              ),
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      context.read<RegisterAccountCubit>().register(
                          email: _emailCtrl.text,
                          username: _usernameCtrl.text,
                          password: _passwordCtrl.text);
                    }
                  },
                  child: Text("Créer le compte"))
            ],
          ),
        ),
      ),
    );
  }
}
