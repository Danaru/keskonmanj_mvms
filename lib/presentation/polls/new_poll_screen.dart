import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:keskonmanj/application/core/date_utils.dart';

import '../../application/core/injection.dart';
import '../../application/poll/failure_ext.dart';
import '../../application/poll/new_poll_cubit.dart';
import '../../application/poll/new_poll_state.dart';
import '../../domain/core/restaurant.dart';
import '../../domain/core/validators.dart';
import '../core/form_fields/date_field.dart';
import '../core/widgets/dialogs_widget.dart';

class NewPollScreen extends StatelessWidget {
  static const DIALOG_KEY = ValueKey('NEW_POLL_DIALOG');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Nouveau sondage"),
        ),
        body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
            child: BlocProvider<NewPollCubit>(
                create: (context) => getIt<NewPollCubit>()..load(),
                child: BlocConsumer<NewPollCubit, NewPollState>(
                  listener: (context, state) {
                    KKMDialog.hide(context, DIALOG_KEY);
                    if (state.submitting) {
                      KKMDialog.show(context,
                          KKMDialog.waiting(title: "Création en cours"),
                          key: DIALOG_KEY);
                    }

                    if (state.created) {
                      context.router.pop();
                    }

                    if (state.onError) {
                      KKMDialog.show(
                          context,
                          KKMDialog.error(
                            errorMessage: state.fail!.message.toString(),
                            key: DIALOG_KEY,
                          ),
                          key: DIALOG_KEY);
                    }
                  },
                  buildWhen: (context, state) {
                    if (state.submitting) return false;
                    return true;
                  },
                  builder: (context, state) {
                    if (state.loaded) {
                      return _NewPollForm(
                        restaurants: state.restaurants,
                      );
                    }

                    if (state.onError) {
                      return Center(
                        child: Text(state.fail.toString()),
                      );
                    }

                    if (state.loading) {
                      return Center(
                          child: Container(
                        height: 50,
                        width: 50,
                        child: CircularProgressIndicator(),
                      ));
                    }

                    return Container();
                  },
                ))));
  }
}

class _NewPollForm extends StatefulWidget {
  final List<Restaurant> restaurants;

  const _NewPollForm({Key? key, required this.restaurants}) : super(key: key);
  @override
  __NewPollFormState createState() => __NewPollFormState();
}

class __NewPollFormState extends State<_NewPollForm> {
  final _formKey = GlobalKey<FormState>();
  DateTime? _forDay;
  List<Restaurant> _restaurantChoices = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            KKMDateFormField(
                labelText: "Pour le ?",
                suffixIcon: Icon(Icons.calendar_today),
                initialValue: minimalDateForNewPoll(),
                minValue: minimalDateForNewPoll(),
                maxValue: minimalDateForNewPoll().add(Duration(days: 5)),
                format: DateFormat('EEE d MMM yyyy',
                    Localizations.localeOf(context).toString()),
                validator: GenericValidators.mandatory,
                onSaved: (date) {
                  setState(() {
                    _forDay = date;
                  });
                }),
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) {
                  final _currentRestaurant = widget.restaurants[index];
                  return _RestaurantTile(
                    onTap: () {
                      if (_restaurantChoices.contains(_currentRestaurant)) {
                        _restaurantChoices.remove(_currentRestaurant);
                      } else {
                        _restaurantChoices.add(_currentRestaurant);
                      }

                      setState(() {
                        _restaurantChoices = _restaurantChoices;
                      });
                    },
                    restaurant: _currentRestaurant,
                    tileColor: _restaurantChoices.contains(_currentRestaurant)
                        ? Colors.grey
                        : null,
                  );
                },
                itemCount: widget.restaurants.length,
              ),
            ),
            if (_restaurantChoices.isNotEmpty)
              ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      context
                          .read<NewPollCubit>()
                          .create(day: _forDay!, choices: _restaurantChoices);
                    }
                  },
                  child: Text("Creer le sondage"))
          ],
        ),
      ),
    );
  }
}

class _RestaurantTile extends StatelessWidget {
  final Restaurant restaurant;
  final VoidCallback onTap;
  final Color? tileColor;

  const _RestaurantTile(
      {Key? key, required this.onTap, required this.restaurant, this.tileColor})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(restaurant.name),
      onTap: onTap,
      tileColor: tileColor ?? null,
    );
  }
}
