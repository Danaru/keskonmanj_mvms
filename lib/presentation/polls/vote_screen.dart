import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pie_chart/pie_chart.dart';
import '../../application/core/date_utils.dart';
import '../../application/core/injection.dart';
import '../../application/poll/vote_cubit.dart';
import '../../application/poll/vote_state.dart';
import '../../domain/core/poll.dart';
import '../../domain/core/restaurant.dart';
import '../../application/poll/failure_ext.dart';

class VoteScreen extends StatelessWidget {
  final String pollID;

  const VoteScreen({Key? key, required this.pollID}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocProvider<VoteCubit>(
      create: (context) => getIt<VoteCubit>()..load(pollID),
      child: BlocBuilder<VoteCubit, VoteState>(
        builder: (context, state) {
          return state.maybeMap(
              loaded: (loadedState) {
                return Scaffold(
                    appBar: AppBar(
                      title: Text(
                          "Sondage du ${formatShortDate(loadedState.poll.day, Localizations.localeOf(context).toString())}"),
                      actions: [
                        if (loadedState.isAdmin &&
                            loadedState.poll.isActive &&
                            !loadedState.userCanVote)
                          IconButton(
                              onPressed: () {
                                context.read<VoteCubit>().generateAndClose();
                              },
                              icon: Icon(Icons.shuffle))
                      ],
                    ),
                    body: loadedState.userCanVote
                        ? _VoteWidget(poll: loadedState.poll)
                        : _VoteResultWidget(
                            poll: loadedState.poll,
                          ));
              },
              error: (errState) => _VoteErrorWidget(
                    errorMessage: errState.fail.message,
                  ),
              orElse: () => _VoteLoadingWidget());
        },
      ),
    );
  }
}

class _VoteLoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: 50,
          height: 50,
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}

class _VoteErrorWidget extends StatelessWidget {
  final String? errorMessage;

  const _VoteErrorWidget({Key? key, this.errorMessage}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Une erreur est survenue :"),
            Text(errorMessage ?? "Iconnue")
          ],
        ),
      ),
    );
  }
}

class _VoteWidget extends StatefulWidget {
  final Poll poll;

  const _VoteWidget({Key? key, required this.poll}) : super(key: key);

  @override
  __VoteWidgetState createState() => __VoteWidgetState();
}

class __VoteWidgetState extends State<_VoteWidget> {
  Restaurant? _selectedRestaurant;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          height: 15,
        ),
        Center(child: Text("Faites votre choix")),
        Expanded(
          child: ListView.builder(
            itemBuilder: (context, index) {
              final restaurant = widget.poll.choices[index];
              return ListTile(
                title: Text("${restaurant.name}"),
                selected: restaurant == _selectedRestaurant ? true : false,
                onTap: () {
                  setState(() {
                    _selectedRestaurant = restaurant;
                  });
                },
              );
            },
            itemCount: widget.poll.choices.length,
          ),
        ),
        if (_selectedRestaurant != null)
          ElevatedButton(
              onPressed: () {
                context.read<VoteCubit>().vote(_selectedRestaurant!);
              },
              child: Text("Voter !"))
      ],
    );
  }
}

class _VoteResultWidget extends StatelessWidget {
  final Poll poll;

  const _VoteResultWidget({Key? key, required this.poll}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Center(
          child: Text(
            poll.isActive ? "Vote en cours ..." : "Winner: ${poll.winner.name}",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        PieChart(dataMap: _pieData()),
        Text("Votants: "),
        Expanded(
          child: ListView.builder(
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text("${poll.votes![index].votantId}"),
                  dense: true,
                );
              },
              itemCount: poll.votes == null ? 0 : poll.votes!.length),
        )
      ],
    );
  }

  Map<String, double> _pieData() {
    final map = <String, double>{};
    map.addEntries(poll.results
        .map<MapEntry<String, double>>(
            (e) => MapEntry(e.restaurant.name, e.points.toDouble()))
        .toList());
    return map;
  }
}
