import 'package:flutter/material.dart';
import '../../application/core/injection.dart';
import '../../application/core/user_session_cubit.dart';
import 'package:auto_route/auto_route.dart';
import '../core/router.gr.dart';

class KKMDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              child: getIt<UserSessionCubit>().state.maybeMap(
                  authentified: (authState) {
                    return Column(
                      children: [
                        Text(authState.currentUser.username),
                        Text(authState.currentUser.email ?? ""),
                      ],
                    );
                  },
                  orElse: () {}),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.logout),
              title: Text("Deconnexion"),
              onTap: () {
                getIt<UserSessionCubit>().logout();
                context.router.pushAndPopUntil(LandingScreenRoute(),
                    predicate: (_) => false);
              },
            ),
          ],
        ),
      ),
    );
  }
}
