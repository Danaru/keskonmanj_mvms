import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../application/core/date_utils.dart';
import '../../application/dashboard/dashboard_cubit.dart';
import '../../application/dashboard/dashboard_state.dart';
import '../../application/core/injection.dart';
import 'drawer.dart';
import 'package:auto_route/auto_route.dart';
import '../core/router.gr.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      drawer: KKMDrawer(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.router.push(NewPollScreenRoute());
        },
        child: Icon(Icons.add),
      ),
      body: BlocProvider<DashboardCubit>(
          create: (context) => getIt<DashboardCubit>()..load(),
          child: PollListWidget()),
    );
  }
}

class PollListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DashboardCubit, DashboardState>(
        builder: (context, state) {
      return state.maybeMap(
          loaded: (loadedState) {
            if (loadedState.polls.isEmpty) {
              return Center(
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Pas de votes"),
                      SizedBox(
                        height: 10,
                      ),
                      IconButton(
                          onPressed: () {
                            context.read<DashboardCubit>().load();
                          },
                          iconSize: 50,
                          color: Colors.blue,
                          icon: Icon(Icons.replay_outlined))
                    ]),
              );
            }
            return ListView.separated(
                itemBuilder: (context, index) {
                  final poll = loadedState.polls[index];
                  return ListTile(
                    title: Text(poll.results.isNotEmpty
                        ? "Resultat: ${poll.winner.name}"
                        : "En cours de vote..."),
                    trailing: poll.isActive
                        ? Icon(
                            Icons.poll,
                            color: Colors.blue,
                          )
                        : Icon(Icons.lock_rounded),
                    isThreeLine: true,
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            "Vote du: ${formatDate(poll.day, Localizations.localeOf(context).toString())}"),
                        Text("Créé par ${poll.creator.username}"),
                      ],
                    ),
                    dense: true,
                    onTap: () {
                      context.router.push(VoteScreenRoute(pollID: poll.id));
                    },
                  );
                },
                separatorBuilder: (context, index) {
                  return SizedBox(
                    height: 5,
                  );
                },
                itemCount: loadedState.polls.length);
          },
          orElse: () => Center(
                child: Container(
                  width: 50,
                  height: 50,
                  child: CircularProgressIndicator(),
                ),
              ));
    });
  }
}
