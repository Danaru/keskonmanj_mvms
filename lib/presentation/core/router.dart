import 'package:auto_route/auto_route.dart';

import '../auth/register_screen.dart';
import '../auth/signin_screen.dart';
import '../dashboard/dashboard_screen.dart';
import '../init/init_screen.dart';
import '../landing/landing_screen.dart';
import '../polls/new_poll_screen.dart';
import '../polls/vote_screen.dart';

@MaterialAutoRouter(routes: [
  MaterialRoute(page: InitScreen, initial: true),
  MaterialRoute(page: LandingScreen),
  MaterialRoute(page: RegisterAccountScreen),
  MaterialRoute(page: SignInScreen),
  MaterialRoute(page: DashboardScreen),
  MaterialRoute(page: NewPollScreen),
  MaterialRoute(page: VoteScreen),
])
class $KKMRouter {}
