import 'package:flutter/material.dart';

import '../../../domain/core/validators.dart';

import '../theme/input_decoration.dart';import 'default_field_padding.dart';

class KKMTextFormField extends StatefulWidget {
  final FocusNode? focusNode;
  final FocusNode? nextFocusNode;
  final String? labelText;
  final String? hintText;
  final InputDecoration? decoration;
  final TextInputType? keyboardType;
  final TextInputAction? keyboardAction;
  final bool autocorrect;
  final Function(String?)? onChanged;
  final int? maxLines;
  final int? minLines;
  final TextEditingController? controller;
  final bool obscureText;
  final String? Function(String?)? validator;
  final AutovalidateMode? autovalidatemode;
  final Widget? suffixIcon;
  final bool enabled;

  const KKMTextFormField(
      {Key? key,
      this.enabled = true,
      this.focusNode,
      this.nextFocusNode,
      this.labelText,
      this.hintText,
      this.decoration,
      this.keyboardType = TextInputType.text,
      this.keyboardAction = TextInputAction.done,
      this.autocorrect = true,
      this.onChanged,
      this.maxLines,
      this.controller,
      this.obscureText = false,
      this.minLines,
      this.validator,
      this.autovalidatemode = AutovalidateMode.onUserInteraction,
      this.suffixIcon})
      : super(key: key);

  factory KKMTextFormField.email({
    TextEditingController? controller,
    FocusNode? nextFocusNode,
    FocusNode? focusNode,
    TextInputAction? keyboardAction,
    String? hintText,
    String? labelText,
    
  }) =>
      KKMTextFormField(
        controller: controller,
        focusNode: focusNode,
        nextFocusNode: nextFocusNode,
        keyboardType: TextInputType.emailAddress,
        keyboardAction: keyboardAction,
        autocorrect: false,
        labelText: labelText ?? "Adresse email",
        hintText: hintText ?? "example@email.com",
        validator: GenericValidators.aggregate([
          GenericValidators.mandatory,
          EmailValidators.validateFormat,
        ]),
      );

  factory KKMTextFormField.password({
    TextEditingController? controller,
    FocusNode? focusNode,
    FocusNode? nextFocusNode,
    String? hintText,
    String? labelText,
    TextInputAction? keyboardAction,
    bool validateLength = false,
  }) =>
      KKMTextFormField(
        controller: controller,
        focusNode: focusNode,
        nextFocusNode: nextFocusNode,
        autocorrect: false,
        keyboardAction: keyboardAction,
        hintText: hintText ?? 'Mot de passe',
        labelText: labelText ?? 'Mot de passe',
        obscureText: true,
        maxLines: 1,
        minLines: 1,
        autovalidatemode: AutovalidateMode.onUserInteraction,
        validator: GenericValidators.aggregate([
          GenericValidators.mandatory,
          if (validateLength) PasswordValidators.minLength
        ]),
      );

  @override
  _KKMTextFormFieldState createState() => _KKMTextFormFieldState();
}

class _KKMTextFormFieldState extends State<KKMTextFormField> {
  bool _obscureText = false;

  @override
  void initState() {
    super.initState();
    _obscureText = widget.obscureText;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultFieldPadding(
        child: Container(
      child: TextFormField(
        enabled: widget.enabled,
        controller: widget.controller,
        obscureText: _obscureText,
        focusNode: widget.focusNode,
        maxLines: widget.maxLines,
        minLines: widget.minLines ?? 1,
        decoration: widget.decoration ??
            KKMInputDecoration.roundedTextInput(
                    hintText: widget.hintText,
                    labelText: widget.labelText,
                    hintStyle: TextStyle(color: Colors.black54),
                    labelStyle: TextStyle(color: Colors.blue))
                .copyWith(
                    suffixIcon: widget.suffixIcon != null
                        ? widget.suffixIcon
                        : widget.obscureText
                            ? IconButton(
                                onPressed: () => setState(() {
                                  _obscureText = !_obscureText;
                                }),
                                icon: Icon(_obscureText
                                    ? Icons.visibility
                                    : Icons.visibility_off),
                              )
                            : null),
        autocorrect: widget.autocorrect,
        keyboardType: widget.keyboardType,
        validator: widget.validator,
        autovalidateMode: widget.autovalidatemode,
        onFieldSubmitted: _onSubmitted,
        textInputAction: widget.keyboardAction,
        onChanged: widget.onChanged,
      ),
    ));
  }

  void _onSubmitted(String? value) {
    widget.nextFocusNode?.requestFocus();
  }
}
