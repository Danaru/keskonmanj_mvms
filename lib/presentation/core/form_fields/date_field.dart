import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../theme/input_decoration.dart';

import 'default_field_padding.dart';

class KKMDateField extends StatelessWidget {
  final DateTime initialValue;
  final DateTime minValue;
  final DateTime maxValue;
  final String? labelText;
  final DateTime? value;
  final Widget? suffixIcon;
  final DateFormat format;
  final Function(DateTime) onChanged;
  final String? errorText;

  const KKMDateField(
      {Key? key,
      required this.initialValue,
      required this.minValue,
      required this.maxValue,
      this.value,
      this.labelText,
      this.suffixIcon,
      required this.format,
      required this.onChanged,
      this.errorText})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return DefaultFieldPadding(
        child: GestureDetector(
      onTap: () async {
        final selectedValue = await _showDatePicker(context);
        if (selectedValue != null) {
          onChanged(selectedValue);
        }
      },
      child: InputDecorator(
        decoration: _getDecoration(),
        child: Text(
          _tryFormat(value, format),
          style: Theme.of(context).textTheme.subtitle1,
        ),
      ),
    ));
  }

  String _tryFormat(DateTime? date, DateFormat format) {
    if (value == null) {
      return '- -';
    }
    try {
      return format.format(date!);
    } catch (e) {
      return date!.millisecondsSinceEpoch.toString();
    }
  }

  Future<DateTime?> _showDatePicker(BuildContext context) async {
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      return await _showCupertinoDatePicker(context);
    }

    return await _showMaterialDatePicker(context);
  }

  Future<DateTime?> _showCupertinoDatePicker(BuildContext context) async {
    DateTime? keep;
    await showModalBottomSheet<DateTime?>(
      context: context,
      builder: (BuildContext builder) {
        return Container(
          height: MediaQuery.of(context).copyWith().size.height / 3,
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            onDateTimeChanged: (date) => keep = date,
            initialDateTime: initialValue,
            use24hFormat: true,
          ),
        );
      },
    );
    return keep;
  }

  Future<DateTime?> _showMaterialDatePicker(BuildContext context) async {
    return await showDatePicker(
      context: context,
      initialDate: initialValue,
      firstDate: minValue,
      lastDate: maxValue,
    );
  }

  InputDecoration _getDecoration() {
    var decoration = KKMInputDecoration.roundedTextInput(
            labelText: labelText,
            hintStyle: TextStyle(color: Colors.blueGrey),
            labelStyle: TextStyle(color: Colors.blueGrey))
        .copyWith(suffixIcon: suffixIcon);

    if (errorText != null) {
      return decoration.copyWith(errorText: errorText);
    }
    return decoration;
  }
}

class KKMDateFormField extends FormField<DateTime?> {
  KKMDateFormField(
      {required FormFieldSetter<DateTime?> onSaved,
      FormFieldValidator<DateTime?>? validator,
      required DateTime initialValue,
      required DateTime minValue,
      required DateTime maxValue,
      String? labelText,
      Widget? suffixIcon,
      AutovalidateMode autovalidateMode = AutovalidateMode.disabled,
      required DateFormat format})
      : super(
            validator: validator,
            builder: (FormFieldState<DateTime?> state) {
              return KKMDateField(
                  format: format,
                  initialValue: initialValue,
                  minValue: minValue,
                  maxValue: maxValue,
                  labelText: labelText,
                  suffixIcon: suffixIcon,
                  onChanged: (value) {
                    state.didChange(value);
                    state.save();
                  },
                  value: state.value,
                  errorText: state.hasError
                      ? validator != null
                          ? validator(state.value)
                          : null
                      : null);
            },
            autovalidateMode: autovalidateMode,
            onSaved: onSaved);
}
