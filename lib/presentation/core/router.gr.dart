// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;

import '../auth/register_screen.dart' as _i5;
import '../auth/signin_screen.dart' as _i6;
import '../dashboard/dashboard_screen.dart' as _i7;
import '../init/init_screen.dart' as _i3;
import '../landing/landing_screen.dart' as _i4;
import '../polls/new_poll_screen.dart' as _i8;
import '../polls/vote_screen.dart' as _i9;

class KKMRouter extends _i1.RootStackRouter {
  KKMRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    InitScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i3.InitScreen();
        }),
    LandingScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i4.LandingScreen();
        }),
    RegisterAccountScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i5.RegisterAccountScreen();
        }),
    SignInScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i6.SignInScreen();
        }),
    DashboardScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i7.DashboardScreen();
        }),
    NewPollScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (_) {
          return _i8.NewPollScreen();
        }),
    VoteScreenRoute.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<VoteScreenRouteArgs>();
          return _i9.VoteScreen(key: args.key, pollID: args.pollID);
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(InitScreenRoute.name, path: '/'),
        _i1.RouteConfig(LandingScreenRoute.name, path: '/landing-screen'),
        _i1.RouteConfig(RegisterAccountScreenRoute.name,
            path: '/register-account-screen'),
        _i1.RouteConfig(SignInScreenRoute.name, path: '/sign-in-screen'),
        _i1.RouteConfig(DashboardScreenRoute.name, path: '/dashboard-screen'),
        _i1.RouteConfig(NewPollScreenRoute.name, path: '/new-poll-screen'),
        _i1.RouteConfig(VoteScreenRoute.name, path: '/vote-screen')
      ];
}

class InitScreenRoute extends _i1.PageRouteInfo {
  const InitScreenRoute() : super(name, path: '/');

  static const String name = 'InitScreenRoute';
}

class LandingScreenRoute extends _i1.PageRouteInfo {
  const LandingScreenRoute() : super(name, path: '/landing-screen');

  static const String name = 'LandingScreenRoute';
}

class RegisterAccountScreenRoute extends _i1.PageRouteInfo {
  const RegisterAccountScreenRoute()
      : super(name, path: '/register-account-screen');

  static const String name = 'RegisterAccountScreenRoute';
}

class SignInScreenRoute extends _i1.PageRouteInfo {
  const SignInScreenRoute() : super(name, path: '/sign-in-screen');

  static const String name = 'SignInScreenRoute';
}

class DashboardScreenRoute extends _i1.PageRouteInfo {
  const DashboardScreenRoute() : super(name, path: '/dashboard-screen');

  static const String name = 'DashboardScreenRoute';
}

class NewPollScreenRoute extends _i1.PageRouteInfo {
  const NewPollScreenRoute() : super(name, path: '/new-poll-screen');

  static const String name = 'NewPollScreenRoute';
}

class VoteScreenRoute extends _i1.PageRouteInfo<VoteScreenRouteArgs> {
  VoteScreenRoute({_i2.Key? key, required String pollID})
      : super(name,
            path: '/vote-screen',
            args: VoteScreenRouteArgs(key: key, pollID: pollID));

  static const String name = 'VoteScreenRoute';
}

class VoteScreenRouteArgs {
  const VoteScreenRouteArgs({this.key, required this.pollID});

  final _i2.Key? key;

  final String pollID;
}
