import 'package:flutter_test/flutter_test.dart';
import 'package:keskonmanj/domain/core/validators.dart';

void main() {
  group("Generic validators", () {
    group("Mandatory validator", () {
      test("Should return string error message if empty", () {
        final String? value = null;
        final expected = 'Ce champ est obligatoire';
        final actual = GenericValidators.mandatory(value);
        expect(actual, expected);
      });

      test("Should return null if not empty", () {
        final String? value = "test";
        final expected = null;
        final actual = GenericValidators.mandatory(value);
        expect(actual, expected);
      });
    });

    group("Agregate validator", () {
      test("Should return null if validators are ok", () {
        final String? Function(String?) validator1 = (value) => null;
        final String? Function(String?) validator2 = (value) => null;

        final expected = null;
        final validators =
            GenericValidators.aggregate<String?>([validator1, validator2]);

        expect(validators.call(null), expected);
      });

      test("Should return good error if first validator is ko", () {
        final String? Function(String?) validator1 = (value) => 'Error1';
        final String? Function(String?) validator2 = (value) => null;

        final expected = 'Error1';
        final validators =
            GenericValidators.aggregate<String?>([validator1, validator2]);

        expect(validators.call(null), expected);
      });

      test("Should return good error if second validator is ko", () {
        final String? Function(String?) validator1 = (value) => null;
        final String? Function(String?) validator2 = (value) => 'Error2';

        final expected = 'Error2';
        final validators =
            GenericValidators.aggregate<String?>([validator1, validator2]);

        expect(validators.call(null), expected);
      });

      test("Should return first error found if many validators are ko", () {
        final String? Function(String?) validator1 = (value) => 'Error1';
        final String? Function(String?) validator2 = (value) => 'Error2';

        final expected = 'Error1';
        final validators =
            GenericValidators.aggregate<String?>([validator1, validator2]);

        expect(validators.call(null), expected);
      });
    });
  });

  group("String validators", () {
    test("Should return bad format if value is not an email address", () {
      final expected = 'Le format ne correspond pas a une adresse email';
      expect(EmailValidators.validateFormat("t"), expected);
      expect(EmailValidators.validateFormat("@t"), expected);
      expect(EmailValidators.validateFormat("@t.t"), expected);
      expect(EmailValidators.validateFormat("t.t"), expected);
      expect(EmailValidators.validateFormat("t.t@t"), expected);
      expect(EmailValidators.validateFormat("t@t"), expected);
    });
    test("Should return null if value is well formatted", () {
      expect(EmailValidators.validateFormat("t@t.t"), null);
    });
  });
}
