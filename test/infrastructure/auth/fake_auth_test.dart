import 'package:flutter_test/flutter_test.dart';
import 'package:keskonmanj/domain/auth/failures.dart';
import 'package:keskonmanj/domain/core/user.dart';
import 'package:keskonmanj/infrastructure/auth/credentials_local_data_source.dart';
import 'package:keskonmanj/infrastructure/auth/auth_facade.dart';
import 'package:keskonmanj/infrastructure/auth/fake_user_remote_data_source.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import './fake_auth_test.mocks.dart';

@GenerateMocks([CredentialsLocalDataSource])
void main() {
  var mockedCredentialsLDS;
  setUpAll(() {
    mockedCredentialsLDS = MockCredentialsLocalDataSource();
    when(mockedCredentialsLDS.saveUser()).thenReturn("");
  });
  group("Register account", () {
    test("Should return user if registration OK", () async {
      final expected = User(
          username: "test3", email: "test3@test.fr", password: "test3pass");
      final actual =
          await AuthFacade(FakeUserRemoteDataSource(), mockedCredentialsLDS)
              .registerAccount(
                  email: expected.email!,
                  username: expected.username,
                  password: expected.password!);

      expect(actual.isRight(), true);
      expect(actual.getOrElse(() => User(username: "badUser")), expected);
    });

    test("Should return unknwon failure if registration KO", () async {
      final actual =
          await AuthFacade(FakeUserRemoteDataSource(), mockedCredentialsLDS)
              .registerAccount(
                  email: "test2@test.fr",
                  username: "test2",
                  password: "test2pass");

      expect(actual.isLeft(), true);
      actual.fold(
        (l) => expect(
          l,
          RegisterAccountFailure.unknown("Oops, a strange error occured"),
        ),
        (_) => expect(true, false, reason: "Should not be a success result !"),
      );
    });
  });
  group("SignIn", () {
    test("Should return bad credentials for unregistered users", () async {
      var actual =
          await AuthFacade(FakeUserRemoteDataSource(), mockedCredentialsLDS)
              .signIn(username: "unknown", password: "unknownpass");
      expect(actual.isLeft(), true);
      actual.fold(
        (fail) => expect(fail, SignInFailure.badCredentials()),
        (_) => expect(true, false, reason: "Should be a failure !"),
      );
    });

    test("Should return bad credentials for bad password", () async {
      var actual =
          await AuthFacade(FakeUserRemoteDataSource(), mockedCredentialsLDS)
              .signIn(username: "test", password: "unknownpass");
      expect(actual.isLeft(), true);
      actual.fold(
        (fail) => expect(fail, SignInFailure.badCredentials()),
        (_) => expect(true, false, reason: "Should be a failure !"),
      );
    });

    test("Should return User for registered users", () async {
      final expected =
          User(username: "test", password: "testpass", email: "test@test.fr");
      var actual =
          await AuthFacade(FakeUserRemoteDataSource(), mockedCredentialsLDS)
              .signIn(username: "test", password: "testpass");
      expect(actual.isRight(), true);
      expect(actual.getOrElse(() => User(username: "badUser")), expected);
    });
  });
}
